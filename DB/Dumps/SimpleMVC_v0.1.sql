CREATE DATABASE  IF NOT EXISTS `SimpleMVC` /*!40100 DEFAULT CHARACTER SET cp1250 */;
USE `SimpleMVC`;
-- MySQL dump 10.15  Distrib 10.0.34-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: SimpleMVC
-- ------------------------------------------------------
-- Server version	10.2.14-MariaDB-10.2.14+maria~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_payables`
--

DROP TABLE IF EXISTS `account_payables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_payables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` double NOT NULL,
  `banks_name` varchar(255) DEFAULT NULL,
  `bik` varchar(255) DEFAULT NULL,
  `corr_account` varchar(255) DEFAULT NULL,
  `date_changed` datetime DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `from_account` int(11) DEFAULT NULL,
  `inn` varchar(255) DEFAULT NULL,
  `kpp` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `pay_account` varchar(255) DEFAULT NULL,
  `payment_code` varchar(255) DEFAULT NULL,
  `payment_description` varchar(255) DEFAULT NULL,
  `payment_due` date DEFAULT NULL,
  `accountPayable_ORDER` varchar(255) DEFAULT NULL,
  `payment_sequence` varchar(255) DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `processed` int(11) NOT NULL DEFAULT 0,
  `processed_date` date DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `to_account` int(11) DEFAULT NULL,
  `transit_account` varchar(255) DEFAULT NULL,
  `objagreement_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_object_agreement_id_idx` (`objagreement_id`),
  CONSTRAINT `FK_object_agreement_id` FOREIGN KEY (`objagreement_id`) REFERENCES `magreement` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_payables`
--

LOCK TABLES `account_payables` WRITE;
/*!40000 ALTER TABLE `account_payables` DISABLE KEYS */;
INSERT INTO `account_payables` VALUES (2,1234.78,'MEGABANK','BIKBIK','32421341234545','2017-04-11 16:21:17','2017-04-11 16:21:17',649,'31412341234','23452345234523452345','NOTE NOTE NOTE','231421341243124312wertwe','qwerqwerqwe34534rqwresdfasdf','SOME DESCRIPTION DESCRIBING DESCRIPTOR','2017-04-11','2134234/2341','3','typetype',0,'2017-04-11',123,123,'asdsadf12341234',NULL),(3,1234.78,'MEGABANK','BIKBIK','32421341234545','2017-04-11 16:23:28','2017-04-11 16:23:28',649,'31412341234','23452345234523452345','NOTE NOTE NOTE','231421341243124312wertwe','qwerqwerqwe34534rqwresdfasdf','SOME DESCRIPTION DESCRIBING DESCRIPTOR','2017-04-11','2134234/2341','3','typetype',0,'2017-04-11',123,123,'asdsadf12341234',NULL);
/*!40000 ALTER TABLE `account_payables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_receivables`
--

DROP TABLE IF EXISTS `account_receivables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_receivables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` double NOT NULL,
  `banks_name` varchar(255) DEFAULT NULL,
  `bik` varchar(255) DEFAULT NULL,
  `corr_account` varchar(255) DEFAULT NULL,
  `date_changed` datetime DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `from_account` int(11) DEFAULT NULL,
  `inn` varchar(255) DEFAULT NULL,
  `kpp` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `pay_account` varchar(255) DEFAULT NULL,
  `payer_id` int(11) DEFAULT NULL,
  `payment_code` varchar(255) DEFAULT NULL,
  `payment_description` varchar(255) DEFAULT NULL,
  `payment_due` date DEFAULT NULL,
  `payment_order` varchar(255) DEFAULT NULL,
  `payment_sequence` varchar(255) DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `processed` int(11) NOT NULL,
  `processed_date` date DEFAULT NULL,
  `to_account` int(11) DEFAULT NULL,
  `transit_account` varchar(255) DEFAULT NULL,
  `objagreement_id` int(11) DEFAULT NULL,
  `accountReceivables_ORDER` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKfcapgmqf16r9pt7p1fc69v9jt` (`objagreement_id`),
  CONSTRAINT `FKfcapgmqf16r9pt7p1fc69v9jt` FOREIGN KEY (`objagreement_id`) REFERENCES `object_agreement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_receivables`
--

LOCK TABLES `account_receivables` WRITE;
/*!40000 ALTER TABLE `account_receivables` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_receivables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agreement_meter`
--

DROP TABLE IF EXISTS `agreement_meter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agreement_meter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_changed` date DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `discount_prcnt` double DEFAULT NULL,
  `initial_value` double DEFAULT NULL,
  `meter_id` int(11) DEFAULT NULL,
  `meter_type_id` int(11) DEFAULT NULL,
  `objagreement_id` int(11) DEFAULT NULL,
  `servcompany_id` int(11) DEFAULT NULL,
  `agreementMeters_ORDER` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8tjen4nulicj0o9ia240pmdx2` (`meter_type_id`),
  KEY `FKnbnah967a96rdpqa6vqq6fvl8` (`objagreement_id`),
  KEY `FKetiaqc1350xp61r9wb5lmcnam` (`servcompany_id`),
  CONSTRAINT `FK8tjen4nulicj0o9ia240pmdx2` FOREIGN KEY (`meter_type_id`) REFERENCES `meter_type` (`id`),
  CONSTRAINT `FKetiaqc1350xp61r9wb5lmcnam` FOREIGN KEY (`servcompany_id`) REFERENCES `service_company` (`id`),
  CONSTRAINT `FKnbnah967a96rdpqa6vqq6fvl8` FOREIGN KEY (`objagreement_id`) REFERENCES `object_agreement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agreement_meter`
--

LOCK TABLES `agreement_meter` WRITE;
/*!40000 ALTER TABLE `agreement_meter` DISABLE KEYS */;
/*!40000 ALTER TABLE `agreement_meter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agreement_type`
--

DROP TABLE IF EXISTS `agreement_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agreement_type` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `type_code` int(11) DEFAULT NULL,
  `agreement_type` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agreement_type`
--

LOCK TABLES `agreement_type` WRITE;
/*!40000 ALTER TABLE `agreement_type` DISABLE KEYS */;
INSERT INTO `agreement_type` VALUES (1,NULL,'rent');
/*!40000 ALTER TABLE `agreement_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apartment_meter`
--

DROP TABLE IF EXISTS `apartment_meter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apartment_meter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_changed` date DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `initial_value` double DEFAULT NULL,
  `meter_id` int(11) DEFAULT NULL,
  `meter_type` int(11) DEFAULT NULL,
  `apartment_id` int(11) DEFAULT NULL,
  `apartmentMeters_ORDER` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6pdrucpxbpsucxtadsj3x6tcx` (`meter_type`),
  KEY `FKckt2590qtg2yke7okrajwyanb` (`apartment_id`),
  CONSTRAINT `FK6pdrucpxbpsucxtadsj3x6tcx` FOREIGN KEY (`meter_type`) REFERENCES `meter_type` (`id`),
  CONSTRAINT `FKckt2590qtg2yke7okrajwyanb` FOREIGN KEY (`apartment_id`) REFERENCES `mobject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apartment_meter`
--

LOCK TABLES `apartment_meter` WRITE;
/*!40000 ALTER TABLE `apartment_meter` DISABLE KEYS */;
/*!40000 ALTER TABLE `apartment_meter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bank_details`
--

DROP TABLE IF EXISTS `bank_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `banks_name` varchar(255) DEFAULT NULL,
  `bik` varchar(255) DEFAULT NULL,
  `corr_account` varchar(255) DEFAULT NULL,
  `inn` varchar(255) DEFAULT NULL,
  `kpp` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `pay_account` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `transit_account` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank_details`
--

LOCK TABLES `bank_details` WRITE;
/*!40000 ALTER TABLE `bank_details` DISABLE KEYS */;
INSERT INTO `bank_details` VALUES (1,'Банк ВТБ (ПАО), ул. Воронцовская, д. 43, стр. 1, г. Москва, 109147 ','Филиал \"Центральный\" Банка ВТБ (ПАО) Г Москва','044525411','30101810145250000411','7702070139','770943002','Корсчет: к/с: 30101810145250000411 в Отделении от Главного управления Центрального банка Российской Федерации по Центральному федеральному округу г. Москва № счета: 30232 8107 0000 2000004 ',NULL,'(495) 777-24-24',NULL),(2,'Россия, Москва, 117997, ул. Вавилова, д. 19 ','СБ РФ Г Москва','044525225','30101810400000000225','7710016640','773601001','Кор. счет 30101810400000000225\nв Главном управлении Центрального банка Российской Федерации по Центральному федеральному округу г. Москва (ГУ Банка России по ЦФО)\nБИК 044525225\nКПП 773601001\nИНН 7707083893','40702810800020106631',NULL,NULL),(3,'Москва, 117997, ул. Вавилова, д. 19 ','ПАО Сбербанк Г Москва','044525225','30101810400000000225','7736520080','773601001','Кор. счет 30101810400000000225\nв Главном управлении Центрального банка Российской Федерации по Центральному федеральному округу г. Москва (ГУ Банка России по ЦФО)\nБИК 044525225\nКПП 773601001\nИНН 7707083893','40702810738360027199','7 (495) 957-57-31',NULL),(4,'121099, г.Москва, 1-й Николощеповский пер., д.6, стр.1','ББР БАНК (АО) Г МОСКВА','044525769','30101810745250000769','3900001002','503201001','2929, внесен в ЕГРЮЛ 29 июля 2002 г., МИМНС № 39 по г. Москве, ОГРН 1027700074775',NULL,'7 (495) 363-91-62',NULL),(5,'109052, Россия, г. Москва, ул. Смирновская, д. 10, строение 22','ПАО \"ПРОМСВЯЗЬБАНК\" Г МОСКВА','044525555','30101810400000000555','7744000912','774301001','Основной государственный регистрационный номер (ОГРН): 1027739019142\nДата внесения записи в Единый государственный реестр юридических лиц (ЕГРЮЛ): 26 июля 2002 г\nКПП: ИФНС России №22 по г. Москва - 772201001',NULL,'(495) 787-33-33',NULL),(6,'115201, г. Москва, Старокаширское шоссе, дом 2, корпус 1, строение 1 ','БАНК ИПБ (АО) Г МОСКВА','044525402','30101810100000000402','7724096412 ','772401001','Код \"S.W.I.F.T.\": INTPRUMM\nДилинговый код \"Reuters\": INPG\nЭлектронная почта: info@ipb.ru\n\nБИК 044525402\nИНН 7724096412\nКПП 772401001\nОКПО 29323770\nОКВЭД 64.19\nК/с 30101810100000000402 в ГУ Банка России по ЦФО',NULL,'7 (495) 411 00 00 ',NULL),(7,'Москва, 123060, 1-й Волоколамский проезд, д. 10, стр. 1','АО \"Тинькофф Банк\"','044525974','30101810145250000974','7710140679','773401001','Лицензия ЦБ РФ на осуществление банковских операций № 2673 от 24 марта 2015 года.\nБанк включен в реестр банков — участников системы обязательного страхования вкладов 24 февраля 2005 года (под номером 696).','30232810100000000004','7 499 605-11-10',NULL),(8,'Россия, 115419, г. Москва, ул. Орджоникидзе, д. 5','ПАО \"МИНБАНК\" Г МОСКВА','044525600','30101810300000000600','7725039953 ','772101001','    Корр. счет: 30101810300000000600\n    Главное управление Центрального банка Российской Федерации по Центральному федеральному округу г. Москва\n    КПП: 997950001\n    БИК: 044525600\n    ИНН: 7725039953\n    ОКПО: 09317135\n    ОКВЭД: 64.19',NULL,'7 (495) 74-000-74',NULL),(9,'142770, г. Москва, п. Сосенское, пос. Газопровод, 101, кор. 5\n','Акционерный Банк «РОССИЯ», центральный филиал','044525220','30101810145250000220','7831000122 ','503701001','ОГРН 1027800000084\nИНН 7831000122\nКПП 775143001\nОКПО 68125115\nБИК 044525220\nКорреспондентский субсчет 30101810145250000220 в ГУ Банка России по ЦФО',NULL,'7 (812) 335 85 05',NULL),(10,'117418, г. Москва, ул. Новочеремушкинская, д. 63.','Банк ГПБ (АО) г. Москва','044525823','30101810200000000823','7744001497','500101001','ИНН: 7744001497 \nКПП: 997950001 \nБИК: 044525823 \nОКПО: 09807684 \nОКВЭД: 64.19         \nОГРН: 1027700167110 от 28.08.2002',NULL,'(495) 913-74-74',NULL),(11,'190000, г. Санкт-Петербург, ул. Большая Морская, д. 29 ',' Банк ВТБ (ПАО) Г Москва','044525187','30101810700000000187','7702070139','770501001','Банк ВТБ (публичное акционерное общество)','40702810900060001086','(495) 777-24-24',NULL),(12,'190000, г. Санкт-Петербург, ул. Большая Морская, д. 29 ',' Банк ВТБ (ПАО) Г Москва','044525187','30101810700000000187','7702070139','770501001','Банк ВТБ (публичное акционерное общество)','40705810200060000027','(495) 777-24-24',NULL);
/*!40000 ALTER TABLE `bank_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (315),(315),(315),(315),(315),(315),(315),(315),(315),(315),(315),(315);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `magreement`
--

DROP TABLE IF EXISTS `magreement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `magreement` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `archived` int(3) DEFAULT 0,
  `agreement_type` int(18) NOT NULL DEFAULT 1,
  `agreement_no` varchar(50) DEFAULT NULL,
  `renter_id` int(18) DEFAULT NULL,
  `moderator_id` int(18) DEFAULT NULL,
  `inventory_file_id` int(18) DEFAULT NULL,
  `agreement_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `service` varchar(250) DEFAULT NULL,
  `insurance_id` int(18) DEFAULT NULL,
  `insurance_no` varchar(50) DEFAULT NULL,
  `insured_from` datetime DEFAULT NULL,
  `insured_till` datetime DEFAULT NULL,
  `unlimited` tinyint(4) NOT NULL DEFAULT 0,
  `period_months` int(11) DEFAULT 0,
  `rate_basis` decimal(10,0) DEFAULT 0,
  `rate_hours` decimal(10,0) DEFAULT 0,
  `payment_date` varchar(10) DEFAULT '01',
  `notice_months` int(11) DEFAULT 0,
  `signed` datetime DEFAULT NULL,
  `notice_received` datetime DEFAULT NULL,
  `comment` varchar(2000) DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `magreements_ORDER` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_agreement_type_idx` (`agreement_type`),
  CONSTRAINT `FK2q5alynqrurt96ubs6mpovtkq` FOREIGN KEY (`agreement_type`) REFERENCES `agreement_type` (`id`),
  CONSTRAINT `FK_agreement_type` FOREIGN KEY (`agreement_type`) REFERENCES `agreement_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `magreement`
--

LOCK TABLES `magreement` WRITE;
/*!40000 ALTER TABLE `magreement` DISABLE KEYS */;
INSERT INTO `magreement` VALUES (2,0,1,'1233214234',12,12,12412,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,0,0,0,0,'01',0,NULL,NULL,NULL,'2017-04-12 15:10:25','2017-04-12 15:10:25',NULL);
/*!40000 ALTER TABLE `magreement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_apartment`
--

DROP TABLE IF EXISTS `SimpleMVC_apartment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_apartment` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `is_deleted` tinyint(1) NOT NULL,
  `renter_id` int(18) DEFAULT NULL,
  `owner_id` int(18) DEFAULT NULL,
  `moderator_id` int(18) DEFAULT NULL,
  `inventory_file_id` int(18) DEFAULT NULL,
  `ll` varchar(255) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `address` varchar(255) NOT NULL,
  `room_number` varchar(5) DEFAULT NULL,
  `rooms` varchar(50) NOT NULL,
  `square` varchar(50) NOT NULL,
  `floor` varchar(50) DEFAULT NULL,
  `isOwner` char(1) NOT NULL,
  `owners` varchar(50) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `service` varchar(50) DEFAULT NULL,
  `insurance` varchar(50) DEFAULT NULL,
  `visiting` varchar(50) DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `amocrm_lead_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=282 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_apartment`
--

LOCK TABLES `SimpleMVC_apartment` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_apartment` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_apartment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_apartment_companies`
--

DROP TABLE IF EXISTS `SimpleMVC_apartment_companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_apartment_companies` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `data` varchar(628) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_apartment_companies`
--

LOCK TABLES `SimpleMVC_apartment_companies` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_apartment_companies` DISABLE KEYS */;
INSERT INTO `SimpleMVC_apartment_companies` VALUES (4,'ООО \"Тест компании\"','БИК 33445566778\nР/С');
/*!40000 ALTER TABLE `SimpleMVC_apartment_companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_apartment_companies_entity`
--

DROP TABLE IF EXISTS `SimpleMVC_apartment_companies_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_apartment_companies_entity` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `date_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `apartment_id` int(18) NOT NULL,
  `company_id` int(18) NOT NULL,
  `payment_code` int(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_apartment_companies_entity`
--

LOCK TABLES `SimpleMVC_apartment_companies_entity` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_apartment_companies_entity` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_apartment_companies_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_apartment_counter`
--

DROP TABLE IF EXISTS `SimpleMVC_apartment_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_apartment_counter` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `date_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_changed` date NOT NULL,
  `apartment_id` int(11) NOT NULL,
  `owner_hvs_value` double NOT NULL,
  `owner_gvs_value` double NOT NULL,
  `owner_electric_value` double NOT NULL,
  `owner_watertap_value` double NOT NULL,
  `renter_hvs_value` double NOT NULL,
  `renter_gvs_value` double NOT NULL,
  `renter_electric_value` double NOT NULL,
  `renter_watertap_value` double NOT NULL,
  `rate_hvs_value` double NOT NULL,
  `rate_gvs_value` double NOT NULL,
  `rate_electric_value` double NOT NULL,
  `rate_watertap_value` double NOT NULL,
  `rate_electric_2_value` double NOT NULL,
  `rate_electric_3_value` double NOT NULL,
  `renter_electric_2_value` double DEFAULT NULL,
  `renter_electric_3_value` double NOT NULL,
  `renter_hvs_2_value` double NOT NULL,
  `owner_hvs_2_value` double NOT NULL,
  `renter_gvs_2_value` double NOT NULL,
  `owner_gvs_2_value` double NOT NULL,
  `rate_hvs_2_value` double NOT NULL,
  `rate_gvs_2_value` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rate_watertap_value` (`rate_watertap_value`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_apartment_counter`
--

LOCK TABLES `SimpleMVC_apartment_counter` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_apartment_counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_apartment_counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_apartment_counter_change`
--

DROP TABLE IF EXISTS `SimpleMVC_apartment_counter_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_apartment_counter_change` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `amount` decimal(18,2) DEFAULT NULL,
  `apartment_id` int(18) DEFAULT NULL,
  `date_changed` datetime DEFAULT NULL,
  `date_filter` varchar(7) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_apartment_counter_change`
--

LOCK TABLES `SimpleMVC_apartment_counter_change` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_apartment_counter_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_apartment_counter_change` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `SimpleMVC_event_type`
--

DROP TABLE IF EXISTS `SimpleMVC_event_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_event_type` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_event_type`
--

LOCK TABLES `SimpleMVC_event_type` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_event_type` DISABLE KEYS */;
INSERT INTO `SimpleMVC_event_type` VALUES (1,'Осмотр','Y'),(2,'Финансы','Y'),(3,'Ремонт','Y');
/*!40000 ALTER TABLE `SimpleMVC_event_type` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `SimpleMVC_payment`
--

DROP TABLE IF EXISTS `SimpleMVC_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_payment` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `apartment_id` int(18) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `pay_day` int(2) DEFAULT NULL,
  `contract_owner` varchar(255) DEFAULT NULL,
  `contract_owner_date` date DEFAULT NULL,
  `contract_owner_expire` date NOT NULL,
  `contract_owner_is_unlimited` varchar(1) NOT NULL DEFAULT 'N',
  `contract_renter` varchar(255) DEFAULT NULL,
  `contract_renter_date` date DEFAULT NULL,
  `contract_renter_from` date NOT NULL,
  `contract_renter_expire` date NOT NULL,
  `rate` varchar(50) NOT NULL,
  `deposit` varchar(50) NOT NULL,
  `tarif_id` int(18) DEFAULT NULL,
  `view` varchar(255) DEFAULT NULL,
  `ndfl` char(1) NOT NULL DEFAULT 'N',
  `insurance_id` int(18) DEFAULT NULL,
  `visit_id` int(18) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_payment`
--

LOCK TABLES `SimpleMVC_payment` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_payment` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `SimpleMVC_payment_debet`
--

DROP TABLE IF EXISTS `SimpleMVC_payment_debet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_payment_debet` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `apartment_id` int(18) DEFAULT NULL,
  `is_payed` varchar(1) NOT NULL DEFAULT 'N',
  `is_transfered` varchar(1) NOT NULL DEFAULT 'N',
  `transfer_value` double DEFAULT NULL,
  `date_filter` varchar(7) DEFAULT '00-0000',
  `date_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_payed` date NOT NULL,
  `date_transfered` date NOT NULL,
  `transfer_file_id` int(18) DEFAULT NULL,
  `transfer_user_id` int(18) NOT NULL,
  `in_archive` tinyint(1) NOT NULL DEFAULT 0,
  `is_reported_overdue` tinyint(1) NOT NULL,
  `is_reported_before` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=387 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_payment_debet`
--

LOCK TABLES `SimpleMVC_payment_debet` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_payment_debet` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_payment_debet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_payment_deposit`
--

DROP TABLE IF EXISTS `SimpleMVC_payment_deposit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_payment_deposit` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `apartment_id` int(18) DEFAULT NULL,
  `file_id` int(18) NOT NULL,
  `event_id` int(18) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `payed` varchar(1) NOT NULL DEFAULT 'N',
  `date_viewed` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `comment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `amount` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_payment_deposit`
--

LOCK TABLES `SimpleMVC_payment_deposit` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_payment_deposit` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_payment_deposit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_payment_fields`
--

DROP TABLE IF EXISTS `SimpleMVC_payment_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_payment_fields` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `date_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `fixed` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `apartment_id` int(18) NOT NULL,
  `rate` double NOT NULL,
  `fixed_value` double NOT NULL,
  `can_be_null` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_payment_fields`
--

LOCK TABLES `SimpleMVC_payment_fields` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_payment_fields` DISABLE KEYS */;
INSERT INTO `SimpleMVC_payment_fields` VALUES (23,0,'2015-09-11 11:08:01','Телефонная связь','N',38,1,0,1),(25,1,'2015-10-02 15:39:38','Переплата','Y',46,0,15,1),(36,0,'2016-01-22 17:36:57','Интернет','Y',68,0,450,0),(31,0,'2015-11-11 10:45:49','Перерасчет','Y',54,0,-5.91,1),(34,0,'2015-12-29 09:35:03','Долг за воду в декабре','Y',63,0,175.2,0);
/*!40000 ALTER TABLE `SimpleMVC_payment_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_payment_invoice`
--

DROP TABLE IF EXISTS `SimpleMVC_payment_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_payment_invoice` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `payed` tinyint(1) NOT NULL DEFAULT 0,
  `amount` decimal(10,2) DEFAULT NULL,
  `user_id` int(18) DEFAULT NULL,
  `date_payed` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=571 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_payment_invoice`
--

LOCK TABLES `SimpleMVC_payment_invoice` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_payment_invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_payment_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_payment_receive`
--

DROP TABLE IF EXISTS `SimpleMVC_payment_receive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_payment_receive` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `apartment_id` int(18) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_filter` varchar(7) NOT NULL DEFAULT '00-0000',
  `type_id` int(18) NOT NULL,
  `amount` double NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `in_archive` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1110 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_payment_receive`
--

LOCK TABLES `SimpleMVC_payment_receive` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_payment_receive` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_payment_receive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_payment_receive_type`
--

DROP TABLE IF EXISTS `SimpleMVC_payment_receive_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_payment_receive_type` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_payment_receive_type`
--

LOCK TABLES `SimpleMVC_payment_receive_type` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_payment_receive_type` DISABLE KEYS */;
INSERT INTO `SimpleMVC_payment_receive_type` VALUES (1,'Коммунальные платежи','Y'),(2,'НДФЛ','Y'),(3,'Комиссия','Y'),(4,'Страхование','Y'),(5,'Ремонт','Y'),(6,'Задержка платежа','Y');
/*!40000 ALTER TABLE `SimpleMVC_payment_receive_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_payment_report`
--

DROP TABLE IF EXISTS `SimpleMVC_payment_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_payment_report` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `apartment_id` int(18) NOT NULL,
  `code` varchar(255) NOT NULL,
  `value` double NOT NULL,
  `amount` double NOT NULL DEFAULT 0,
  `date_create` timestamp NULL DEFAULT NULL,
  `date_filter` varchar(7) NOT NULL DEFAULT '00-0000',
  `payed` varchar(1) NOT NULL DEFAULT 'N',
  `in_archive` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2022 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_payment_report`
--

LOCK TABLES `SimpleMVC_payment_report` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_payment_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_payment_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_request_snyat`
--

DROP TABLE IF EXISTS `SimpleMVC_request_snyat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_request_snyat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `apartment_address` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_second_name` varchar(255) NOT NULL,
  `referance` text NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `rent_first_time` varchar(255) NOT NULL,
  `user_pets` varchar(255) NOT NULL,
  `user_rent_date` varchar(255) NOT NULL,
  `user_change_current` varchar(255) NOT NULL,
  `user_smoking` varchar(255) NOT NULL,
  `robot` varchar(255) NOT NULL,
  `amocrm_lead_id` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_request_snyat`
--

LOCK TABLES `SimpleMVC_request_snyat` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_request_snyat` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_request_snyat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_service`
--

DROP TABLE IF EXISTS `SimpleMVC_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_service` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `date_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `name` varchar(255) NOT NULL,
  `active` char(1) DEFAULT NULL,
  `sort` int(18) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_service`
--

LOCK TABLES `SimpleMVC_service` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_service` DISABLE KEYS */;
INSERT INTO `SimpleMVC_service` VALUES (1,'2015-02-05 19:05:18','Стиральная машина','Y',40),(2,'2015-02-05 19:05:18','Посудомоечная машина','Y',60),(3,'2015-02-05 19:05:34','Можно с животными','Y',50),(4,'2015-02-05 19:05:34','Балкон','Y',10),(5,'2015-02-05 19:05:49','Телефон','Y',70),(6,'2015-02-05 19:05:49','Кондиционер','Y',100),(7,'2015-02-05 19:06:08','Телевизор','Y',80),(8,'2015-02-05 19:06:08','Холодильник','Y',20),(9,'2015-02-05 19:06:16','Интернет','Y',90),(10,'2015-05-08 11:20:09','Можно с детьми','Y',30);
/*!40000 ALTER TABLE `SimpleMVC_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_sms_log`
--

DROP TABLE IF EXISTS `SimpleMVC_sms_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_sms_log` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL,
  `text` varchar(1024) DEFAULT NULL,
  `apartment_id` int(18) DEFAULT NULL,
  `user_id` int(18) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_sms_log`
--

LOCK TABLES `SimpleMVC_sms_log` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_sms_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_sms_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_subscribe`
--

DROP TABLE IF EXISTS `SimpleMVC_subscribe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_subscribe` (
  `date_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8 NOT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `confimed` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=156 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_subscribe`
--

LOCK TABLES `SimpleMVC_subscribe` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_subscribe` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_subscribe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_tarif`
--

DROP TABLE IF EXISTS `SimpleMVC_tarif`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_tarif` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `date_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `name` varchar(255) NOT NULL,
  `active` char(1) DEFAULT NULL,
  `perc_acoomp` varchar(255) NOT NULL,
  `perc_NDFL` varchar(255) NOT NULL,
  `perc_view_monthly` varchar(255) NOT NULL,
  `perc_view_monthly_double` varchar(255) NOT NULL,
  `comment` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_tarif`
--

LOCK TABLES `SimpleMVC_tarif` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_tarif` DISABLE KEYS */;
INSERT INTO `SimpleMVC_tarif` VALUES (1,'2015-02-07 12:34:35','Базовый','Y','7%','0%','0%','0%',''),(8,'2015-05-13 11:44:17','Каникулы','Y','7%','0%','0%','0%','Первые 3 месяца без коммиссии ');
/*!40000 ALTER TABLE `SimpleMVC_tarif` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_user`
--

DROP TABLE IF EXISTS `SimpleMVC_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_user` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `date_register` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_login` datetime DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `second_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `birthday` varchar(15) DEFAULT NULL,
  `sex` char(1) DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `keyword` varchar(50) DEFAULT NULL,
  `code_time` datetime DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `code_try_count` int(1) NOT NULL DEFAULT 0,
  `password` varchar(255) DEFAULT NULL,
  `group_id` char(1) DEFAULT NULL,
  `avatar_id` int(18) DEFAULT NULL,
  `admin_comment` varchar(255) DEFAULT NULL,
  `amocrm_user_id` bigint(20) DEFAULT NULL,
  `role_code` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=520 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_user`
--

LOCK TABLES `SimpleMVC_user` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_user` DISABLE KEYS */;
INSERT INTO `SimpleMVC_user` VALUES (11,0,'2015-02-28 13:44:24','2015-05-08 21:43:45','Somebody','admin','Somebody','Somebody',NULL,'M','9999999999','Somebody@mail.ru',NULL,'0000-00-00 00:00:00',NULL,0,'$2a$04$VTzUJBRzwKn5eBCWu/ykgudn/L7lkdxoxLn3ZUORc5P4PXtT9SCOC','1',300,'',0,1);
/*!40000 ALTER TABLE `SimpleMVC_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_user_checking_account`
--

DROP TABLE IF EXISTS `SimpleMVC_user_checking_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_user_checking_account` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `user_id` int(18) NOT NULL,
  `number` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `bank` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `inn` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `bik` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `fio` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_user_checking_account`
--

LOCK TABLES `SimpleMVC_user_checking_account` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_user_checking_account` DISABLE KEYS */;
INSERT INTO `SimpleMVC_user_checking_account` VALUES (4,98,'40817810430016798908','ЗАО КБ \"Ситибанк\"','7710401987','044525202','Трескин Алексей Алексеевич','г.Москва'),(5,123,'123','123123123','112312','321','123131','1233123123123');
/*!40000 ALTER TABLE `SimpleMVC_user_checking_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_user_doc`
--

DROP TABLE IF EXISTS `SimpleMVC_user_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_user_doc` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `user_id` int(18) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `serial` varchar(255) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `who_issued` varchar(255) DEFAULT NULL,
  `when_issued` varchar(255) DEFAULT NULL,
  `registration` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=126 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_user_doc`
--

LOCK TABLES `SimpleMVC_user_doc` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_user_doc` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_user_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_user_group`
--

DROP TABLE IF EXISTS `SimpleMVC_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_user_group` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_user_group`
--

LOCK TABLES `SimpleMVC_user_group` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_user_group` DISABLE KEYS */;
INSERT INTO `SimpleMVC_user_group` VALUES (1,'Администратор','1'),(2,'Менеджер','1'),(3,'Подтвержденный пользователь','2'),(4,'Пользователь','2'),(5,'Заблокирован','3'),(6,'Контент-менеджер','1');
/*!40000 ALTER TABLE `SimpleMVC_user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_user_settings`
--

DROP TABLE IF EXISTS `SimpleMVC_user_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_user_settings` (
  `id` int(18) NOT NULL,
  `user_id` int(18) NOT NULL,
  `notify_payment_sms` tinyint(1) NOT NULL,
  `notify_payment_email` tinyint(1) NOT NULL,
  `notify_visit_sms` tinyint(1) NOT NULL,
  `notify_visit_email` tinyint(1) NOT NULL,
  `notify_news_sms` tinyint(1) NOT NULL,
  `notify_news_email` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_user_settings`
--

LOCK TABLES `SimpleMVC_user_settings` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_user_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_user_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_user_sync`
--

DROP TABLE IF EXISTS `SimpleMVC_user_sync`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_user_sync` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `user_id` int(18) NOT NULL,
  `service_name` text NOT NULL,
  `is_complete` tinyint(1) DEFAULT 0,
  `date_create` timestamp NULL DEFAULT current_timestamp(),
  `date_sync` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lead_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_user_sync`
--

LOCK TABLES `SimpleMVC_user_sync` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_user_sync` DISABLE KEYS */;
/*!40000 ALTER TABLE `SimpleMVC_user_sync` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SimpleMVC_visit`
--

DROP TABLE IF EXISTS `SimpleMVC_visit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SimpleMVC_visit` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SimpleMVC_visit`
--

LOCK TABLES `SimpleMVC_visit` WRITE;
/*!40000 ALTER TABLE `SimpleMVC_visit` DISABLE KEYS */;
INSERT INTO `SimpleMVC_visit` VALUES (3,'Каждые 3 месяца','Y');
/*!40000 ALTER TABLE `SimpleMVC_visit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meter_type`
--

DROP TABLE IF EXISTS `meter_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meter_type` (
  `id` int(11) NOT NULL,
  `date_create` datetime DEFAULT NULL,
  `measure_unit` varchar(255) DEFAULT NULL,
  `metertype_code` varchar(255) DEFAULT NULL,
  `metertype_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `metertype_code_UNIQUE` (`metertype_code`),
  UNIQUE KEY `metertype_description_UNIQUE` (`metertype_description`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meter_type`
--

LOCK TABLES `meter_type` WRITE;
/*!40000 ALTER TABLE `meter_type` DISABLE KEYS */;
INSERT INTO `meter_type` VALUES (28,'2017-03-30 07:58:48','mt/3','GVS1','gorjachaia voda 1'),(31,'2017-03-30 08:17:41','mt/3','GVS2','gorjachaia voda - 2');
/*!40000 ALTER TABLE `meter_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meters_records`
--

DROP TABLE IF EXISTS `meters_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meters_records` (
  `id` int(11) NOT NULL,
  `date_changed` datetime DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `processed` tinyint(4) NOT NULL,
  `processed_date` date DEFAULT NULL,
  `registered` date DEFAULT NULL,
  `value` double NOT NULL,
  `agreementMeter_id` int(11) DEFAULT NULL,
  `metersRecords_ORDER` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4gtgiyc51kv9tup54uy2wefo1` (`agreementMeter_id`),
  CONSTRAINT `FK4gtgiyc51kv9tup54uy2wefo1` FOREIGN KEY (`agreementMeter_id`) REFERENCES `agreement_meter` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meters_records`
--

LOCK TABLES `meters_records` WRITE;
/*!40000 ALTER TABLE `meters_records` DISABLE KEYS */;
/*!40000 ALTER TABLE `meters_records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobject`
--

DROP TABLE IF EXISTS `mobject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobject` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `object_type` int(3) NOT NULL DEFAULT 1,
  `archived` int(3) NOT NULL,
  `owner_id` int(18) DEFAULT NULL,
  `inventory_file_id` int(18) DEFAULT NULL,
  `year_built` timestamp NOT NULL DEFAULT current_timestamp(),
  `address_id` int(11) NOT NULL DEFAULT 1,
  `address` varchar(255) NOT NULL,
  `room_number` varchar(5) DEFAULT NULL,
  `rooms` varchar(50) NOT NULL,
  `square` varchar(50) NOT NULL,
  `floor` varchar(50) DEFAULT NULL,
  `landline_phone` varchar(45) DEFAULT NULL,
  `isOwner` varchar(2) NOT NULL,
  `isNDFL` varchar(45) DEFAULT NULL,
  `owners` varchar(50) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `service` varchar(50) DEFAULT NULL,
  `is_insured` int(11) DEFAULT 1,
  `insurance_police_no` varchar(255) DEFAULT NULL,
  `visiting` varchar(50) DEFAULT NULL,
  `MGTS_password` varchar(45) DEFAULT NULL,
  `Doorlock_code` varchar(45) DEFAULT NULL,
  `comment` varchar(4000) DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `amocrm_lead_id` int(2) DEFAULT NULL,
  `mobjects_ORDER` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_object_type_idx` (`object_type`),
  CONSTRAINT `FK_object_type` FOREIGN KEY (`object_type`) REFERENCES `object_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=314 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobject`
--

LOCK TABLES `mobject` WRITE;
/*!40000 ALTER TABLE `mobject` DISABLE KEYS */;
/*!40000 ALTER TABLE `mobject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `msubject`
--

DROP TABLE IF EXISTS `msubject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `msubject` (
  `id` int(11) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `admin_comment` varchar(255) DEFAULT NULL,
  `amocrm_user_id` decimal(19,2) DEFAULT NULL,
  `archived` tinyint(4) NOT NULL,
  `avatar_id` int(11) DEFAULT NULL,
  `bank_details_id` int(11) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `code_time` datetime DEFAULT NULL,
  `code_try_count` int(11) DEFAULT NULL,
  `date_register` datetime DEFAULT NULL,
  `discount_basis` varchar(255) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `group_id` varchar(255) DEFAULT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `payment_for` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `second_name` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `subject_type` int(11) DEFAULT NULL,
  `msubjects_ORDER` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1tregyn0lxgq52vq3c0db5j93` (`subject_type`),
  CONSTRAINT `FK1tregyn0lxgq52vq3c0db5j93` FOREIGN KEY (`subject_type`) REFERENCES `subject_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `msubject`
--

LOCK TABLES `msubject` WRITE;
/*!40000 ALTER TABLE `msubject` DISABLE KEYS */;
/*!40000 ALTER TABLE `msubject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `object_agreement`
--

DROP TABLE IF EXISTS `object_agreement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_agreement` (
  `id` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `agreement_id` int(11) DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `objectAgreements_ORDER` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKdmnh4lqq3dn27sclx7cyj8d6s` (`agreement_id`),
  KEY `FKi5w2u12l9so4wmgwq7m8tl2b4` (`object_id`),
  CONSTRAINT `FKdmnh4lqq3dn27sclx7cyj8d6s` FOREIGN KEY (`agreement_id`) REFERENCES `magreement` (`id`),
  CONSTRAINT `FKi5w2u12l9so4wmgwq7m8tl2b4` FOREIGN KEY (`object_id`) REFERENCES `mobject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `object_agreement`
--

LOCK TABLES `object_agreement` WRITE;
/*!40000 ALTER TABLE `object_agreement` DISABLE KEYS */;
/*!40000 ALTER TABLE `object_agreement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `object_type`
--

DROP TABLE IF EXISTS `object_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_type` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `objecttype_description` varchar(255) NOT NULL,
  `date_create` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `objecttype_description_UNIQUE` (`objecttype_description`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `objecttype_description` (`objecttype_description`)
) ENGINE=InnoDB AUTO_INCREMENT=315 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `object_type`
--

LOCK TABLES `object_type` WRITE;
/*!40000 ALTER TABLE `object_type` DISABLE KEYS */;
INSERT INTO `object_type` VALUES (1,'Жилое помещение в личной собственности','2017-03-13 12:24:37'),(2,'машиноместо','2017-03-13 12:24:37');
/*!40000 ALTER TABLE `object_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_company`
--

DROP TABLE IF EXISTS `service_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_company` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `INN` varchar(45) NOT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_company`
--

LOCK TABLES `service_company` WRITE;
/*!40000 ALTER TABLE `service_company` DISABLE KEYS */;
INSERT INTO `service_company` VALUES (1,1,'ГБУ \"Жилищник Гагаринского района\"','119333, Москва, ул. Ленинский просп., дом 60/2','7736247137',1,'2698142000931110','7 495 930-06-81'),(2,1,'ГБУ \"Жилищник Алексеевского района\"','129164, г.Москва, ул.Маломосковская, дом 6, кор.2','7717799012',1,'2694142000800918','495 682 45 74'),(3,1,'Жилищно-строительный кооператив \"Мир-3\"','улица Бутлерова, дом 2, корпус 2','7728069794',3,NULL,'8 (495) 336-81-32'),(4,1,'ООО \"Гранат\"','129281, Москва, ул.Л.Бабушкина, дом 31, кор.2','',1,'Бабушкинский район',NULL),(5,1,'ТСЖ \"Панферова 7-2\"','119261, Москва, ул.Панферова дом7, кор.2','7702000406',1,'Гагаринский район','8 (499) 134-58-92'),(6,1,'ООО \"Вискан\"','119361, Москва, ул.Б.Очаковская, дом 47А, стр.1','7743510614',1,'Хорошевский район','7 495 730-52-07'),(7,1,'ГБУ \"Жилищник Пресненского района\"','123022, Москва, ул. Красная Пресня, дом 26, стр.1','7703820360',1,'ОДС №10','+7 495 605 -74-53'),(8,2,'ООО \"Юг Девелопмент\"','117105, Москва, Варшавское шоссе  д.9, стр.6','7726583210',3,NULL,'8 (495) 952-20-28'),(9,2,'ООО \"КРЦ\"','г. Одинцово, ул. Кутузовская, д.31','5032246197',4,NULL,NULL),(10,1,'ГБУ \"Жилищник района Покровское-Стрешнево\"','125362, Москва, уд. Свободы д.15/10','773301001',1,NULL,'7 499 492-29-36'),(11,1,'ООО \"ЖилСтройСервис\"','115432, Москва, 2-ой Южнопортовый пр., д.5 кор.1','7723553352',1,NULL,'8-499-236-24-10'),(12,1,'ООО \"ЭЛИТСТРОЙ\"','105187, Москва, Погонный проезд, дом 5, кор.3','7719859154',1,'район Богородское','+7 499 162-77-89'),(13,1,'ГБУ \"Жилищник Пресненского района\"','123022, Москва, ул. Красная Пресня, дом 26, стр.1','7703820360',1,'Пресненский район','+7 495 605 -74-53'),(14,1,'ГБУ \"Жилищник района Хамовники\"','Москва, ул.Фрунзенская 3-я, дом 19','7704880121',1,'http://gbu-zhilishnik-hamovniki.ru/struktura.html','+7 499 2425204'),(15,1,'ГБУ \"Жилищник Нижегородского района\"','Москва, ул. Нижегородская, д.58, кор.2','7722315281',1,'2697142000900904','+7 495 6716263'),(16,1,'ГБУ \"Жилищник района Ясенево\"','Москва, ул. Айвазовского, д.8, кор.2','7702070139',1,'ГБУ \" Жилищник района Ясенево\" является членом Некоммерческого партнерства «Объединение организаций в области профессионального управления недвижимостью «ГАРАНТИЯ» г. тел.: 8 (499) 124-61-17, сайт в Интернете www.sro-garanty.ru','8 (499) 124-61-17'),(17,2,'ТСЖ \" Трехгорка\"','МО, г.Одинцово, ул. Чистяковой, д.2','5032143226',3,'http://tsjtrehgorka.ru/','(495) 589-38-00'),(18,1,'ГБУ \"Жилищник района Люблино\"','Москва, ул. Кубанская, дом 27','7723876836',1,NULL,'+7 495 350-27-60'),(19,1,'ГБУ \"Жилищник Даниловского района\"','115093, Москва, ул. 3-й Павловский переулок, д.10','7725816790',1,'ГБУ «Жилищник Даниловского района» создано 1 марта 2014  №146-ПП от 14.03.2013 г.  № 672-ПП от 10.10.2013 г. «О внесении изменений в постановление Правительства Москвы от 14.03.2013 г.» и распоряжении Префектуры ЮАО №01-41-645 от 15.10.2013 г.','8 (499) 237-43-68'),(20,1,'ГБУ \"Жилищник Нижегородского района\"','Москва, ул. Нижегородская, д.58, кор.2','7722315281',1,'2697142000900904\nОсновной государственный регистрационный номер (ОГРН):	1157746065454\nДата присвоения ОГРН:	30.01.2015\nИНН:	7722315281','+7 495 671-62-63'),(21,2,'ООО \"ЭКСперт-Сервис\"','125212, Москва, ул. Адмирала Макарова, д.6, стр.3','7712105447',5,'http://gc-expert.pro/about/','+7 495 609-68-59'),(22,1,'ГБУ \"Жилищник района Тверской\"','127051, Москва, Цветной бульвар, 15, кор.2','7707807190',1,'http://www.gbu-tverskoy.ru/','+7 (495)624-71-72'),(23,1,'ГБУ \"Жилищник Пресненского района\"','123022, Москва, ул. Красная Пресня, дом 26, стр.1','7703820360',1,'http://gbu-presnya.ru/','+7 495 605 -74-53'),(24,2,'ЖСК \"Орехово-4\"','115551, Москва, Шипиловский пр., л.49/1',' 7737055501',6,'http://gorod.mos.ru/index.php?show=organization&id=1597131','8 (495) 392-74-90'),(25,5,'ФКР города Москвы','101000, Москва, ул.Маросейка, д.11/4, стр.3','7701090559',1,'3781464000452018\nИНН: 7701090559\nКПП: 770101001\nОКПО: 420020?1\nОГРН: 1157700003230\nОКФС: 13 - Собственность субъектов Российской Федерации\nОКОГУ: 4210014','8 (495) 633-69-74'),(26,2,'ООО \"Светлый Город\"','117997, Москва, ул. Вавилова, д.69','7736669467',7,'https://sw-g.ru/kontakty.html','+7 (495) 649-66-89'),(27,2,'ЖСК \"Жулебино\"','109145, Москва, ул. Привольная, д.13, кор.1','7721076993',8,'http://gorod.mos.ru/index.php?show=organization&id=1598736','(495) 705-12-39'),(30,1,'ГБУ \"Жилищник района Фили-Давыдково\"','121108, Москва, ул. Олеко Дундича, д.34','7731288482',1,'http://www.dez-fd.ru/','+7 (499) 144-95-45'),(31,1,'АО \"Управляющая Компания Гольяново\"','107065, Москва, ул. Уральская, д.23, кор.2','7718959371',1,'http://укгольяново.рф/','+7 (495) 467-05-04'),(32,1,'ГБУ \"Жилищник района Текстильщики\"','109129, Москва, ул. Текстильщиков 8-я, д.16, кор.1','7723894401',1,'http://tekstilschiki.uvaogbu.ru/','+7 499 178-18-78'),(33,1,'ГБУ \"ЭВАЖД\"','121059 Москва. Ул. 2-я Бородинская, д.17','7730199840',1,'ИНН/КПП: 7730199840/ 773001001\nОГРН: 1167746299820\nОКПО: 05130486\nОКОГУ: 2300230\nОКТМО: 45318000000\nОКВЭД: 68.32.1\nОКФС: 13\nОКОПФ: 75203','(499) 243-07-27'),(34,1,'ГБУ \"Жилищник района Зюзино\"','117452, Москва, Симферопольский бульвар, д.16, кор.1','7727846494',1,'2698142000931100\nhttp://zhilishnikzuzino.ru/ ','+7 495 318-86-36'),(36,1,'ГБУ \"Жилищник района Беговой\"','125040 Москва, ул. Расковой , 14','7714343246',1,'Лицензия на право управления МКД Москвы от 07.07.2015 № 077 000702.\nОсновной государственный регистрационный номер (ОГРН):	1157746528830\nДата присвоения ОГРН:	11.06.2015\nИНН:	7714343246','+7 495 613-28-63'),(37,2,'ЖСК \"Видный\"','117321, Москва, ул. Профсоюзная, д.138','7728118530',3,'ОГРН   	  1037739015159    присвоен: 05.01.2003\nИНН   	  7728118530\nКПП   	  772801001\nОКПО  	  33651451\nОКТМО 	  45907000000','429-05-45 '),(39,2,'ТСЖ \"СИГМА\"','Москва, ул. Серпуховская Б., д. 25, к. 2','7705403876',3,'Полное наименование 	Товарищество собственников жилья «Сигма»\nПредседатель 	Контарович Олег Рафаилович\nАдрес 	Москва, ул. Серпуховская Б., д. 25, к. 2\nИНН 	7705403876\nОГРН 	1027739788416\nДействует с 	15.06.2001\nУчредители 	Валерий Васильевич Стебаков','8-926-221-85-85'),(40,2,'ООО \"УК Комфорт Сити\"','119270, Москва, Лужнецкая наб., д.2/4, стр.6','7704829710',1,'https://www.uk-kc.ru/','+7 (499) 426-46-46'),(41,1,'ГБУ \"Жилищник Ярославского района\"','129337, Москва, ул. Ярославское шоссе, д.120А','7716789300',1,'http://gbuyar.ru/rekvizity/','+7 499 188-43-77'),(42,2,'ООО \"Митино-1\"','125430, Москва, Пятницкое шоссе, д.21, оф.XXXI','7716769960',3,'ОГРН  	  1147746296653    присвоен: 19.03.2014\nИНН   	  7716769960\nКПП   	  773301001\nОКПО   	  29178291\nОКТМО  	  45367000000','+7 495 212-1060'),(44,2,'АО \"Единый расчетный центр \"Управдом\"','107023, Москва, ул. Б.Семеновская, д.32, стр.7','7720605034',3,'https://upravdoma.ru/site/login/','+7 498 500-39-89'),(46,2,'ООО \"Красногорье-ДЭЗ\"','143408, ОБЛАСТЬ МОСКОВСКАЯ, КРАСНОГОРСКИЙ РАЙОН, ГОРОД КРАСНОГОРСК, УЛИЦА ЛЕНИНА, 47, 1 ','5024110442',3,'\nОГРН: 1105024001543\nИНН: 5024110442\nКПП: 502401001\nПФР: 060022013644\nОКПО: 66086598\nФСС: 502311066250231\nОКАТО: 46223501000\nOKTMO: 46623101\nОКОГУ: 4210014\nОКФС: 16\nОКОПФ: 12300\n','8(495)662-33-54'),(48,1,'ООО \"МосОблЕИРЦ\"','140002 Московская область, г. Люберцы, Октябрьский проспект, д. 1 (бизнес-центр «Лермонтовский», 6 этаж, ком. 602).','5037008735',9,'Капремонт\nИНН 5037008735\nКПП 503701001\nЗарегистрировано 26.06.2013 г. Межрайонной ИФНС России №11 по МО\nСвидетельство о регистрации серия 50 №010462581\nОГРН 1135043002962\nОКАТО 46467000000\nОКПО 45697369 ','8 (495) 374-51-61'),(49,5,'ТСЖ \" Трехгорка\" ( спец. счет на кап. рем.)','МО, г.Одинцово, ул. Чистяковой, д.2','5032143226',3,'Капремонт\nПолное наименование банка \nв Сберегательном Банке России, Вернадское отделение №7970 г. Москва\nРасчетный счет 40703810138180133914\nКорреспондентский счет 30101810400000000225\nБИК 0445252','8 (495) 589-32-89'),(50,5,'Фонд капитального ремонта','143912\nМосковская обл г Балашиха\nпр-кт Ленина, 6 оф 55','7701169833',10,'ИНН   7701169833\nКПП   500101001\nОГРН  1137799018081\nДата образования: 2 октября 2013\n','8 (495) 601-95-15'),(51,2,'ООО \"СМАРТ СЕРВИС\"','Москва, Народная ул, д.14, стр.3, оф.7','7734555231',11,'ЖКХ','8-495-225-65-27'),(53,5,'ТСЖ \"СИГМА\"','115093, г Москва, улица Серпуховская Б., дом 25 КОРПУС 2','7705403876',3,'Капремонт\nОГРН   	  1027739788416    присвоен: 16.12.2002\nИНН   	  7705403876\nКПП   	  770501001\nОКПО   	  56641314\nОКТМО  	  45376000000','8-926-221-85-85'),(54,2,'ООО \"ЙЕС-Москва\"','Москва, Пятницкое шоссе , д.21, пом ХХХI','7733901048',3,'ОГРН   	  5147746309453    присвоен: 05.11.2014\nИНН   	  7733901048\nКПП   	  773301001\nОКПО   	  17574768\nОКТМО   	  45367000000',NULL);
/*!40000 ALTER TABLE `service_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_company_type`
--

DROP TABLE IF EXISTS `service_company_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_company_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank` varchar(255) DEFAULT NULL,
  `BIK` varchar(255) DEFAULT NULL,
  `checking_account` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `corr_account` varchar(255) DEFAULT NULL,
  `inn` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_company_type`
--

LOCK TABLES `service_company_type` WRITE;
/*!40000 ALTER TABLE `service_company_type` DISABLE KEYS */;
INSERT INTO `service_company_type` VALUES (1,'Филиал \"Центральный\" Банка ВТБ (ПАО) Г Москва','044525411','','ЖКХ Жилищника','30101810145250000411','7702070139');
/*!40000 ALTER TABLE `service_company_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicecompany_banks`
--

DROP TABLE IF EXISTS `servicecompany_banks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicecompany_banks` (
  `id` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `isprimary` int(11) NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `serviceCompany_id` int(11) DEFAULT NULL,
  `servicecompanyBanks_ORDER` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKdkx0nl6q6whvtqhqu531w2eco` (`bank_id`),
  KEY `FKnxvped5gw690rkrbgphr0ltep` (`serviceCompany_id`),
  CONSTRAINT `FKdkx0nl6q6whvtqhqu531w2eco` FOREIGN KEY (`bank_id`) REFERENCES `bank_details` (`id`),
  CONSTRAINT `FKnxvped5gw690rkrbgphr0ltep` FOREIGN KEY (`serviceCompany_id`) REFERENCES `service_company` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicecompany_banks`
--

LOCK TABLES `servicecompany_banks` WRITE;
/*!40000 ALTER TABLE `servicecompany_banks` DISABLE KEYS */;
INSERT INTO `servicecompany_banks` VALUES (1,NULL,1,NULL,1,1,NULL),(2,NULL,1,NULL,1,2,NULL),(3,NULL,1,NULL,3,3,NULL),(4,NULL,1,NULL,1,4,NULL),(5,NULL,1,NULL,1,5,NULL),(6,NULL,1,NULL,1,6,NULL),(7,NULL,1,NULL,1,7,NULL),(8,NULL,1,NULL,3,8,NULL),(9,NULL,1,NULL,4,9,NULL),(10,NULL,1,NULL,1,10,NULL),(11,NULL,1,NULL,1,11,NULL),(12,NULL,1,NULL,1,12,NULL),(13,NULL,1,NULL,1,13,NULL),(14,NULL,1,NULL,1,14,NULL),(15,NULL,1,NULL,1,15,NULL),(16,NULL,1,NULL,1,16,NULL),(17,NULL,1,NULL,3,17,NULL),(18,NULL,1,NULL,1,18,NULL),(19,NULL,1,NULL,1,19,NULL),(20,NULL,1,NULL,1,20,NULL),(21,NULL,1,NULL,5,21,NULL),(22,NULL,1,NULL,1,22,NULL),(23,NULL,1,NULL,1,23,NULL),(24,NULL,1,NULL,6,24,NULL),(25,NULL,1,NULL,1,25,NULL),(26,NULL,1,NULL,7,26,NULL),(27,NULL,1,NULL,8,27,NULL),(28,NULL,0,NULL,1,27,NULL),(30,NULL,1,NULL,1,30,NULL),(31,NULL,1,NULL,1,31,NULL),(32,NULL,1,NULL,1,32,NULL),(33,NULL,1,NULL,1,33,NULL),(34,NULL,1,NULL,1,34,NULL),(35,NULL,1,NULL,1,36,NULL),(36,NULL,1,NULL,3,37,NULL),(37,NULL,1,NULL,3,39,NULL),(38,NULL,1,NULL,1,40,NULL),(39,NULL,1,NULL,1,41,NULL),(40,NULL,1,NULL,3,42,NULL),(41,NULL,1,NULL,3,44,NULL),(42,NULL,1,NULL,3,46,NULL),(43,NULL,1,NULL,9,48,NULL),(44,NULL,1,NULL,3,49,NULL),(45,NULL,1,NULL,10,50,NULL),(46,NULL,1,NULL,11,51,NULL),(47,NULL,0,NULL,12,51,NULL),(48,NULL,1,NULL,3,53,NULL),(49,NULL,1,NULL,3,54,NULL);
/*!40000 ALTER TABLE `servicecompany_banks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject_type`
--

DROP TABLE IF EXISTS `subject_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject_type` (
  `id` int(11) NOT NULL,
  `subject_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subject_type_UNIQUE` (`subject_type`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject_type`
--

LOCK TABLES `subject_type` WRITE;
/*!40000 ALTER TABLE `subject_type` DISABLE KEYS */;
INSERT INTO `subject_type` VALUES (2,'Арендатор'),(1,'Владелец'),(3,'Менеджер Марум');
/*!40000 ALTER TABLE `subject_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects_banks`
--

DROP TABLE IF EXISTS `subjects_banks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subjects_banks` (
  `id` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `isprimary` int(11) NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subjectsBanks_ORDER` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9af74nff8txipnrwivwomis23` (`bank_id`),
  KEY `FKrljo7uf1am98ip5dog1rbmpi7` (`subject_id`),
  CONSTRAINT `FK9af74nff8txipnrwivwomis23` FOREIGN KEY (`bank_id`) REFERENCES `bank_details` (`id`),
  CONSTRAINT `FKrljo7uf1am98ip5dog1rbmpi7` FOREIGN KEY (`subject_id`) REFERENCES `msubject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects_banks`
--

LOCK TABLES `subjects_banks` WRITE;
/*!40000 ALTER TABLE `subjects_banks` DISABLE KEYS */;
/*!40000 ALTER TABLE `subjects_banks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tariff`
--

DROP TABLE IF EXISTS `tariff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tariff` (
  `id` int(11) NOT NULL,
  `active_from` date DEFAULT NULL,
  `active_to` date DEFAULT NULL,
  `date_changed` date DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `rate` double NOT NULL,
  `meterType_id` int(11) DEFAULT NULL,
  `serviceCompany_id` int(11) DEFAULT NULL,
  `tariffs_ORDER` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7woxl9jkbinxc8sttb3gavfy1` (`meterType_id`),
  KEY `FKisoc9ipolkkg2k19yrk6osjc5` (`serviceCompany_id`),
  CONSTRAINT `FK7woxl9jkbinxc8sttb3gavfy1` FOREIGN KEY (`meterType_id`) REFERENCES `meter_type` (`id`),
  CONSTRAINT `FKisoc9ipolkkg2k19yrk6osjc5` FOREIGN KEY (`serviceCompany_id`) REFERENCES `service_company` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tariff`
--

LOCK TABLES `tariff` WRITE;
/*!40000 ALTER TABLE `tariff` DISABLE KEYS */;
/*!40000 ALTER TABLE `tariff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'SimpleMVC'
--

--
-- Dumping routines for database 'SimpleMVC'
--

--
-- Final view structure for view `SimpleMVC_get_request_snyat`
--

/*!50001 DROP TABLE IF EXISTS `SimpleMVC_get_request_snyat`*/;
/*!50001 DROP VIEW IF EXISTS `SimpleMVC_get_request_snyat`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `SimpleMVC_get_request_snyat` AS select `SimpleMVC_request_snyat`.`id` AS `id`,`SimpleMVC_request_snyat`.`apartment_address` AS `apartment_address`,`SimpleMVC_request_snyat`.`user_name` AS `user_name`,`SimpleMVC_request_snyat`.`user_second_name` AS `user_second_name`,`SimpleMVC_request_snyat`.`referance` AS `referance`,`SimpleMVC_request_snyat`.`user_email` AS `user_email`,`SimpleMVC_request_snyat`.`user_phone` AS `user_phone`,`SimpleMVC_request_snyat`.`rent_first_time` AS `rent_first_time`,`SimpleMVC_request_snyat`.`user_pets` AS `user_pets`,`SimpleMVC_request_snyat`.`user_rent_date` AS `user_rent_date`,`SimpleMVC_request_snyat`.`user_change_current` AS `user_change_current`,`SimpleMVC_request_snyat`.`user_smoking` AS `user_smoking`,`SimpleMVC_request_snyat`.`amocrm_lead_id` AS `amocrm_lead_id`,`SimpleMVC_request_snyat`.`createdAt` AS `createdAt` from `SimpleMVC_request_snyat` where `SimpleMVC_request_snyat`.`is_deleted` = 0 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-17 18:43:01
