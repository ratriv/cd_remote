package com.SimpleMVC.commons;

public class SimpleMVCException extends Exception {

    public static final String NOT_ALLOWED = "not.allowed";
    public static final String ACCESS_DENIED = "access.denied";
    public static final String USER_REQUIRED = "user.required";
    public static final String UNKNOWN_ROLE = "unknown.role";
    public static final String MOBJECT_REQUIRED = "mobject.required";
    public static final String SUBJECT_REQUIRED = "subject.required";

    public SimpleMVCException(String message) {
        super(message);
    }
}
