package com.SimpleMVC.local;

import static org.junit.Assert.fail;

import com.SimpleMVC.JBossLoginContextFactory;
import com.SimpleMVC.dto.UserDto;
import com.SimpleMVC.model.Role;
import com.SimpleMVC.model.User;
import com.SimpleMVC.orika.OrikaBeanMapper;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;

import java.security.PrivilegedAction;
import java.util.function.Supplier;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.transaction.UserTransaction;

public abstract class AbstractTest {

    @PersistenceContext(unitName = "SimpleMVCJPA")
    private EntityManager em;

    @Inject
    protected UserTransaction transaction;

    public static Archive<?> createDeployment(Package aPackage, Class... classes) {
        return ShrinkWrap.create(JavaArchive.class).addPackage(aPackage).addClasses(classes)
            .addClass(OrikaBeanMapper.class)
            .addAsManifestResource("META-INF/test-persistence.xml", "persistence.xml")
            .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    protected User newUser() throws Exception {
        User user = new User();
        user.setLogin("test01");
        user.setPassword("test01");
        user.setName("test01");
        user.setCodeTryCount(1);
        user.setGroupId("0");
        user.setPhone("+7900");
        user.setSalt("testsalt");

        user.setRole(Role.MANAGER);
        transaction.begin();
        em.persist(user);
        transaction.commit();
        return user;
    }

    public UserDto newUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setLogin(user.getLogin());
        userDto.setPassword(user.getPassword());
        userDto.setName(user.getName());
        userDto.setCodeTryCount(user.getCodeTryCount());
        userDto.setGroupId(user.getGroupId());
        userDto.setPhone(user.getPhone());
        userDto.setSalt(user.getSalt());
        return userDto;
    }

    public void secured(Supplier<Void> action) {
        LoginContext loginContext = null;
        try {
            loginContext = JBossLoginContextFactory.createLoginContext("user1", "password");
            loginContext.login();
            Subject.doAs(loginContext.getSubject(), (PrivilegedAction<Void>) action::get);
        } catch (LoginException e) {
            fail("Cannot login");
        } finally {
            if (loginContext != null) {
                try {
                    loginContext.logout();
                } catch (LoginException e) {
                    fail("Cannot logout.");
                }
            }
        }
    }
}
