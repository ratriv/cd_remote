package com.SimpleMVC.local;

import static org.junit.Assert.fail;

import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap;
import com.SimpleMVC.JBossLoginContextFactory;
import com.SimpleMVC.commons.SimpleMVCException;
import com.SimpleMVC.dto.QueryRequest;
import com.SimpleMVC.dto.UserDto;
//import com.SimpleMVC.dto.legacy.CreateAgreement;
import com.SimpleMVC.ejb.UserServiceImpl;
import com.SimpleMVC.model.ObjectWithId;
import com.SimpleMVC.model.User;
import com.SimpleMVC.orika.OrikaBeanMapper;
import com.SimpleMVC.orika.converters.IdToObjectConverter;
import com.thoughtworks.paranamer.Paranamer;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.cern.colt.map.OpenIntObjectHashMap;
import ma.glasnost.orika.constructor.SimpleConstructorResolverStrategy;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.converter.builtin.BuiltinConverters;
import ma.glasnost.orika.impl.ConfigurableMapper;
import ma.glasnost.orika.impl.generator.CodeGenerationStrategy;
import ma.glasnost.orika.impl.generator.specification.AbstractSpecification;
import ma.glasnost.orika.impl.mapping.strategy.MappingStrategyRecorder;
import ma.glasnost.orika.impl.util.StringUtil;
import ma.glasnost.orika.inheritance.DefaultSuperTypeResolverStrategy;
import ma.glasnost.orika.metadata.Type;
import ma.glasnost.orika.property.IntrospectorPropertyResolver;
import ma.glasnost.orika.unenhance.UnenhanceStrategy;
import ma.glasnost.orika.util.HashMapUtility;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RunWith(Arquillian.class)
public class UserServiceTest extends AbstractTest {

    @PersistenceContext(unitName = "SimpleMVCJPA")
    private EntityManager em;

    @Inject
    private UserService userService;

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(JavaArchive.class).addPackage(UserService.class.getPackage())
            .addClass(JBossLoginContextFactory.class)
            .addPackage(UserServiceImpl.class.getPackage())
            .addPackage(ObjectWithId.class.getPackage())
            .addPackage(SimpleMVCException.class.getPackage())
           // .addPackage(CreateAgreement.class.getPackage())
            .addPackage(QueryRequest.class.getPackage())
            .addClass(OrikaBeanMapper.class)
            .addPackage(ConfigurableMapper.class.getPackage())
            .addPackage(MapperFacade.class.getPackage())
            .addPackage(Type.class.getPackage())
            .addPackage(DefaultSuperTypeResolverStrategy.class.getPackage())
            .addPackage(ConverterFactory.class.getPackage())
            .addPackage(CodeGenerationStrategy.class.getPackage())
            .addPackage(UnenhanceStrategy.class.getPackage())
            .addPackage(HashMapUtility.class.getPackage())
            .addPackage(ConcurrentLinkedHashMap.class.getPackage())
            .addPackage(SimpleConstructorResolverStrategy.class.getPackage())
            .addPackage(Paranamer.class.getPackage())
            .addPackage(IntrospectorPropertyResolver.class.getPackage())
            .addPackage(AbstractSpecification.class.getPackage())
            .addPackage(OpenIntObjectHashMap.class.getPackage())
            .addPackage(BuiltinConverters.class.getPackage())
            .addPackage(MappingStrategyRecorder.class.getPackage())
            .addPackage(StringUtil.class.getPackage())
            .addPackage(IdToObjectConverter.class.getPackage())
            .addAsManifestResource("META-INF/test-persistence.xml", "persistence.xml")
            .addAsManifestResource("META-INF/jboss-ejb3.xml")
            .addAsResource("users.properties")
            .addAsResource("roles.properties")
            .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Before
    public void init() throws Exception {
        transaction.begin();
        em.joinTransaction();
        em.createQuery("delete from User").executeUpdate();
        transaction.commit();
    }

    public User newUser() {
        User user = new User();
        user.setLogin("test01");
        user.setPassword("test01");
        user.setName("test01");
        user.setCodeTryCount(1);
        user.setGroupId("0");
        user.setPhone("+7900");
        user.setSalt("testsalt");
        return user;
    }

    @Test
    public void create() {
        secured(() -> {
            try {
                User user = newUser();
                UserDto userDto = newUserDto(user);

                Assert.assertNull(userDto.getId());
                UserDto savedUser = userService.save(userDto);

                Assert.assertNotNull(savedUser);
                Assert.assertNotNull(savedUser.getId());
            } catch (Exception e) {
                fail();
            }

            return null;
        });
    }

    @Test
    public void selectUserByLogin() throws SimpleMVCException {
        secured(() -> {
            try {
                userService.save(newUserDto(newUser()));
            } catch (Exception e) {
                fail();
            }

            return null;
        });

        UserDto user = userService.findByLogin("test01");
        Assert.assertNotNull(user);
    }

    @Test
    public void updateUser() {
        secured(() -> {
            try {
                userService.save(newUserDto(newUser()));

                UserDto user = userService.findByLogin("test01");
                user.setPassword("newPassword");
                userService.save(user);

                UserDto saved = userService.findByLogin("test01");
                Assert.assertEquals("newPassword", saved.getPassword());
            } catch (Exception e) {
                fail();
            }

            return null;
        });
    }

    @Test(expected = SimpleMVCException.class)
    public void notFoundUser() throws SimpleMVCException {
        UserDto user = userService.findByLogin("unknown_user");
        Assert.assertNull(user);
    }
}
