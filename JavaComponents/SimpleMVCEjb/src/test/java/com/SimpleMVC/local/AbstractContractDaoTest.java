package com.SimpleMVC.local;

import static org.junit.Assert.fail;

import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap;
import com.SimpleMVC.JBossLoginContextFactory;
import com.SimpleMVC.commons.SimpleMVCException;
import com.SimpleMVC.dto.QueryRequest;
import com.SimpleMVC.dto.UserDto;
//import com.SimpleMVC.dto.legacy.CreateAgreement;
//import com.SimpleMVC.ejb.AccountPayableServiceImpl;
//import com.SimpleMVC.model.AccountPayable;
import com.SimpleMVC.model.User;
import com.SimpleMVC.orika.OrikaBeanMapper;
import com.thoughtworks.paranamer.Paranamer;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.text.RandomStringGenerator;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;

import java.security.PrivilegedAction;
import java.util.function.Supplier;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

public abstract class AbstractContractDaoTest<ID, Model, R extends CrudService<Model, ID>> extends AbstractTest {

    @PersistenceContext(unitName = "SimpleMVCJPA")
    private EntityManager em;

    protected User user;
    protected UserDto userDto;

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
           // .addPackage(AccountPayableService.class.getPackage())
            .addClass(JBossLoginContextFactory.class)
           // .addPackage(AccountPayable.class.getPackage())
           // .addPackage(AccountPayableServiceImpl.class.getPackage())
           // .addPackage(CreateAgreement.class.getPackage())
            .addPackage(SimpleMVCException.class.getPackage())
            .addPackage(UserDto.class.getPackage())
            // Orika
            .addPackages(true, OrikaBeanMapper.class.getPackage())
            .addPackages(true, MapperFacade.class.getPackage())
            .addPackages(true, ConcurrentLinkedHashMap.class.getPackage())
            .addPackages(true, Paranamer.class.getPackage())
            // avoid ClassNotFoundException
            .addPackages(true, ObjectUtils.class.getPackage())
            .addPackages(true, RandomStringGenerator.class.getPackage())
            .addAsWebInfResource("META-INF/test-persistence.xml", "classes/META-INF/persistence.xml")
            .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
            .addAsWebInfResource("META-INF/jboss-ejb3.xml")
            .addAsResource("users.properties")
            .addAsResource("roles.properties");
    }

    public void before(String modelName) throws Exception {
        transaction.begin();
        em.joinTransaction();
        em.createQuery("delete from " + modelName).executeUpdate();
        transaction.commit();
        user = newUser();
        userDto = newUserDto(user);
    }

    protected abstract Model createModel() throws Exception;

    protected abstract R getContractRepository();

    protected abstract ID getId(Model model);

    protected abstract ID idToDelete();

    @Test
    public void create() {
        secured(() -> {
            try {
                Model model = createModel();
                Assert.assertNotNull(model);
                Assert.assertNotNull(getId(model));
            } catch (Exception e) {
                fail();
            }
            return null;
        });
    }

    @Test
    public void delete() throws SimpleMVCException {
        secured(() -> {
            try {
                getContractRepository().delete(idToDelete());
            } catch (SimpleMVCException e) {
                // expected SimpleMVCException
            } catch (Exception e) {
                fail();
            }
            return null;
        });
    }

    @Test
    public void testLast() throws Exception {
        secured(() -> {
            try {
                createModel();
                Model model = createModel();
                Model last = getContractRepository().last();
                Assert.assertNotNull(last);
                Assert.assertEquals(getId(model), getId(last));
            } catch (Exception e) {
                fail();
            }

            return null;
        });
    }

    @Test
    public void testList() {
        secured(() -> {
            try {
                createModel();
                createModel();
                createModel();

                Assert.assertNotNull(getContractRepository().list(new QueryRequest()));
                Assert.assertEquals(3, getContractRepository().list(new QueryRequest()).size());
            } catch (Exception e) {
                fail();
            }
            return null;
        });
    }

    @Test
    public void getByIdTest() {
        secured(() -> {
            try {
                Model model = createModel();
                Assert.assertNotNull(getContractRepository().getById(getId(model)));
            } catch (Exception e) {
                fail();
            }
            return null;
        });
    }
}
