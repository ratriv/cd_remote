package com.SimpleMVC.local;

import com.SimpleMVC.dto.BankDetailDto;
import com.SimpleMVC.model.BankDetail;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Before;
import org.junit.runner.RunWith;

import javax.inject.Inject;

@RunWith(Arquillian.class)
public class BankDetailDaoTest extends AbstractContractDaoTest<Integer, BankDetailDto, BankDetailService> {

    @Inject
    private BankDetailService bankDetailService;

    @Before
    public void init() throws Exception {
        before(BankDetail.class.getSimpleName());
    }

    @Override
    protected BankDetailDto createModel() throws Exception {
        BankDetailDto bankDetail = new BankDetailDto();
        return bankDetailService.create(bankDetail);
    }

    @Override
    protected BankDetailService getContractRepository() {
        return bankDetailService;
    }

    @Override
    protected Integer getId(BankDetailDto bankDetailDto) {
        return bankDetailDto.getId();
    }

    @Override
    protected Integer idToDelete() {
        return 1;
    }
}
