package com.SimpleMVC.local;

import com.SimpleMVC.dto.BankDetailDto;

import javax.ejb.Local;

@Local
public interface BankDetailService extends CrudService<BankDetailDto, Integer> {

}
