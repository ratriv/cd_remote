package com.SimpleMVC.local;

import com.SimpleMVC.commons.SimpleMVCException;
import com.SimpleMVC.dto.UserDto;

import java.util.Set;
import javax.ejb.Local;

@Local
public interface UserService {

    UserDto findByLogin(String login) throws SimpleMVCException;

    UserDto save(UserDto user) throws SimpleMVCException;

    Set<String> loadRoles(UserDto user) throws SimpleMVCException;

    void saveToken(String token, UserDto user) throws SimpleMVCException;

    String getSalt(String token) throws SimpleMVCException;
}
