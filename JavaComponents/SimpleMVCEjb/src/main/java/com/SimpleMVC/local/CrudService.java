package com.SimpleMVC.local;

import com.SimpleMVC.commons.SimpleMVCException;
import com.SimpleMVC.dto.QueryRequest;

import java.util.List;

public interface CrudService<E, ID> {

    E getById(ID id) throws SimpleMVCException;

    List<E> list(QueryRequest request) throws SimpleMVCException;

    E last() throws SimpleMVCException;

    E create(E object) throws SimpleMVCException;

    boolean delete(ID id) throws SimpleMVCException;
}
