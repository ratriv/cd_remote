package com.SimpleMVC.orika;

import static javax.ejb.TransactionAttributeType.REQUIRED;

import com.SimpleMVC.orika.converters.IdToObjectConverter;
//import com.SimpleMVC.orika.converters.IntToArgreementTypeCodeConverter;
import com.SimpleMVC.orika.converters.IntToRoleCodeConverter;
import com.SimpleMVC.orika.converters.StringToSecTokenConverter;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = REQUIRED)
@Stateless
public class OrikaBeanMapper extends ConfigurableMapper {

    @PersistenceContext(unitName = "SimpleMVCJPA")
    private EntityManager em;

    public OrikaBeanMapper() {
        super(false);
    }

    @Override
    protected void configure(final MapperFactory factory) {
        Objects.requireNonNull(em);
        factory.getConverterFactory().registerConverter(new IdToObjectConverter(em));
        factory.getConverterFactory().registerConverter(new StringToSecTokenConverter(em));
    //    factory.getConverterFactory().registerConverter(new IntToArgreementTypeCodeConverter());
        factory.getConverterFactory().registerConverter(new IntToRoleCodeConverter());
    }

    @Override
    protected void configureFactoryBuilder(final DefaultMapperFactory.Builder factoryBuilder) {
        factoryBuilder.mapNulls(true);
    }

    @PostConstruct
    protected void init() {
        super.init();
    }
}
