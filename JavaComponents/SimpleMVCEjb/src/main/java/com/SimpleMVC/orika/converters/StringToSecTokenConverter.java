package com.SimpleMVC.orika.converters;

import javax.persistence.EntityManager;

import com.SimpleMVC.model.SecToken;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

public class StringToSecTokenConverter extends BidirectionalConverter<String, SecToken> {

	private EntityManager em;

	public StringToSecTokenConverter(EntityManager em) {
		this.em = em;
	}

	@Override
	public SecToken convertTo(String source, Type<SecToken> destinationType, MappingContext mappingContext) {
		return em.find(destinationType.getRawType(), source);
	}

	@Override
	public String convertFrom(SecToken source, Type<String> destinationType, MappingContext mappingContext) {
		return source.getToken();
	}

}
