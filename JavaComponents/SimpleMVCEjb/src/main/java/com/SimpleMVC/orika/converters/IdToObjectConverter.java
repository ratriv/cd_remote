package com.SimpleMVC.orika.converters;

import javax.persistence.EntityManager;

import com.SimpleMVC.model.ObjectWithId;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

public class IdToObjectConverter<T extends ObjectWithId> extends BidirectionalConverter<Integer, T> {

	private EntityManager em;

	public IdToObjectConverter(EntityManager em) {
		this.em = em;
	}

	@Override
	public Integer convertFrom(T arg0, Type<Integer> arg1, MappingContext arg2) {
		return arg0.getId();
	}

	@Override
	public T convertTo(Integer arg0, Type<T> arg1, MappingContext arg2) {
		return arg0 != null ? em.find(arg1.getRawType(), arg0) : null;
	}

	@Override
	public boolean canConvert(Type<?> sourceType, Type<?> destinationType) {
		return (this.sourceType.isAssignableFrom(sourceType) && this.destinationType.isAssignableFrom(destinationType))
				|| (this.destinationType.isAssignableFrom(sourceType)
						&& this.sourceType.isAssignableFrom(destinationType));
	}

}
