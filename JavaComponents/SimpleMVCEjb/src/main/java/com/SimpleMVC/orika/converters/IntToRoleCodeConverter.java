package com.SimpleMVC.orika.converters;

import com.SimpleMVC.model.Role;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

public class IntToRoleCodeConverter extends BidirectionalConverter<Integer, Role> {

    @Override
    public Integer convertFrom(Role arg0, Type<Integer> arg1, MappingContext arg2) {
        return arg0.ordinal();
    }

    @Override
    public Role convertTo(Integer arg0, Type<Role> arg1, MappingContext arg2) {
        return Role.values()[arg0];
    }
}
