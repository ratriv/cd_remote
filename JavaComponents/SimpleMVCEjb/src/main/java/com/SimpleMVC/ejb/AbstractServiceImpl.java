package com.SimpleMVC.ejb;

import com.SimpleMVC.commons.SimpleMVCException;
import com.SimpleMVC.local.CrudService;
import com.SimpleMVC.orika.OrikaBeanMapper;
import org.slf4j.Logger;

import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

public abstract class AbstractServiceImpl<E, P, ID> implements CrudService<E, ID> {

    @PersistenceContext(unitName = "SimpleMVCJPA")
    private EntityManager em;

    @Inject
    private Logger logger;

    @Inject
    private OrikaBeanMapper mapper;

    private final Class<E> dtoClass;

    private final Class<P> persistentClass;

    @SuppressWarnings("unchecked")
    protected AbstractServiceImpl() {
        ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
        this.dtoClass = (Class<E>) type.getActualTypeArguments()[0];
        this.persistentClass = (Class<P>) type.getActualTypeArguments()[1];
    }

    protected EntityManager getEntityManager() {
        return em;
    }

    protected OrikaBeanMapper getMapper() {
        return mapper;
    }

    protected Class<E> getDtoClass() {
        return dtoClass;
    }

    protected Class<P> getPersistentClass() {
        return persistentClass;
    }

    @Override
    public E getById(ID id) throws SimpleMVCException {
        return getMapper().map(getEntityManager().find(getPersistentClass(), id), getDtoClass());
    }

    protected List<P> invokeQuery(String query) {
        return getEntityManager().createNamedQuery(query, getPersistentClass()).getResultList();
    }

    protected List<P> invokeLikeQuery(String query, String field, Object value) {
        return getEntityManager().createNamedQuery(query, getPersistentClass()).setParameter(field, "%" + value + "%").getResultList();
    }

    protected List<P> invokeEqualQuery(String query, String field, Object value) {
        return getEntityManager().createNamedQuery(query, getPersistentClass()).setParameter(field, value).getResultList();
    }

    protected List<P> invokeDateQuery(String query, Date from, Date to) {
        return getEntityManager().createNamedQuery(query, getPersistentClass()).setParameter("dateFrom", from).setParameter("dateTo", to)
            .getResultList();
    }

    protected E last(String query) throws SimpleMVCException {
        try {
            return getMapper().map(getEntityManager().createNamedQuery(query, getPersistentClass()).getSingleResult(), getDtoClass());
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public boolean delete(ID id) throws SimpleMVCException {
        throw new SimpleMVCException("UNSUPPORTED");
    }

    protected abstract ID getId(P entity);

    @Override
    public E create(E object) throws SimpleMVCException {
        P entity = getMapper().map(object, getPersistentClass());
        EntityManager entityManager = getEntityManager();
        if (getId(entity) == null) {
            entityManager.persist(entity);
        } else {
            entityManager.merge(entity);
        }
        entityManager.flush();
        return getMapper().map(entity, getDtoClass());
    }
}
