package com.SimpleMVC.ejb;

import com.SimpleMVC.commons.SimpleMVCException;
import com.SimpleMVC.dto.BankDetailDto;
//import com.SimpleMVC.dto.MetersRecordDto;
import com.SimpleMVC.dto.QueryRequest;
import com.SimpleMVC.local.BankDetailService;
import com.SimpleMVC.model.BankDetail;
import com.SimpleMVC.model.Role;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

import static com.SimpleMVC.dto.QueryRequest.Filter.ACCOUNT;
import static com.SimpleMVC.dto.QueryRequest.Filter.ADDRESS;
import static com.SimpleMVC.dto.QueryRequest.Filter.COMPANY_NAME;
import static com.SimpleMVC.dto.QueryRequest.Filter.COUNTER_TYPE;
import static com.SimpleMVC.dto.QueryRequest.Filter.SIGNED;
import static javax.ejb.TransactionAttributeType.REQUIRED;

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = REQUIRED)
public class BankDetailServiceImpl extends AbstractServiceImpl<BankDetailDto, BankDetail, Integer> implements BankDetailService {

    @Inject
    private Logger logger;

    @Override
    public List<BankDetailDto> list(QueryRequest request) throws SimpleMVCException {

        List<BankDetail> result;
        if (StringUtils.isBlank(request.getField())) {
            result = findAll();
        } else {
            String field = request.getField().trim();
            switch (field) {
                case ADDRESS:
                    result = findByAddress(request.getName());
                    break;
                case COMPANY_NAME:
                    result = findBySubjectName(request.getName());
                    break;
                case COUNTER_TYPE:
                    result = findByBankName(request.getName());
                    break;
                case ACCOUNT:
                    result = findByAccount(request.getName());
                    break;
                case SIGNED:
                    result = findBySigned(request.getFrom(), request.getTo());
                    break;
                default:
                    logger.error("Unknown field: " + field);
                    throw new SimpleMVCException("BANK_DETAIL.UNKNOWN_FILTER_FIELD");
            }
        }
        return getMapper().mapAsList(result, BankDetailDto.class);
    }

    @RolesAllowed({Role.Name.SERVICE_ADMIN, Role.Name.MANAGER})
    protected List<BankDetail> findAll() {
        return invokeQuery("BankDetail.findAll");
    }

    @RolesAllowed({Role.Name.SYSTEM_ADMIN, Role.Name.MANAGER, Role.Name.OWNER, Role.Name.RENTER})
    protected List<BankDetail> findByBankName(String name) {
        return getEntityManager().createNamedQuery("BankDetail.findByBankName", BankDetail.class).setParameter("name", name)
                .getResultList();
    }

    @RolesAllowed({Role.Name.SYSTEM_ADMIN, Role.Name.MANAGER})
    protected List<BankDetail> findByAccount(String name) {
        return getEntityManager().createNamedQuery("BankDetail.findByAccount", BankDetail.class).setParameter("name", name).getResultList();
    }

    @RolesAllowed({Role.Name.SYSTEM_ADMIN, Role.Name.MANAGER, Role.Name.OWNER, Role.Name.RENTER})
    protected List<BankDetail> findByAddress(String address) {
        return invokeLikeQuery("BankDetail.findByAddress", "address", address);
    }

    @RolesAllowed({Role.Name.SYSTEM_ADMIN, Role.Name.MANAGER, Role.Name.OWNER, Role.Name.RENTER})
    protected List<BankDetail> findBySubjectName(String subjectName) {
        return invokeLikeQuery("BankDetail.findBySubjectName", "name", subjectName);
    }

    @RolesAllowed({Role.Name.SYSTEM_ADMIN, Role.Name.MANAGER})
    protected List<BankDetail> findBySigned(Date from, Date to) {
        return invokeDateQuery("BankDetail.findBySigned", from, to);
    }

    @RolesAllowed({Role.Name.SYSTEM_ADMIN, Role.Name.MANAGER})
    @Override
    public BankDetailDto create(BankDetailDto object) throws SimpleMVCException {
        return super.create(object);
    }

    @RolesAllowed({Role.Name.SYSTEM_ADMIN, Role.Name.MANAGER})
    @Override
    public boolean delete(Integer integer) throws SimpleMVCException {
        return super.delete(integer);
    }

    @Override
    public BankDetailDto last() throws SimpleMVCException {
        return super.last("BankDetail.getLast");
    }

    @RolesAllowed({Role.Name.SYSTEM_ADMIN, Role.Name.MANAGER})
    @Override
    protected Integer getId(BankDetail entity) {
        return entity.getId();
    }

    @RolesAllowed({Role.Name.SYSTEM_ADMIN, Role.Name.MANAGER})
    @Override
    public BankDetailDto getById(Integer integer) throws SimpleMVCException {
        return super.getById(integer);
    }
}
