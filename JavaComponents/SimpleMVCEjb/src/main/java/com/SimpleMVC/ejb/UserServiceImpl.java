package com.SimpleMVC.ejb;


import com.SimpleMVC.commons.SimpleMVCException;
import com.SimpleMVC.dto.UserDto;
import com.SimpleMVC.local.UserService;
import com.SimpleMVC.model.AgreementTypeCode;
import com.SimpleMVC.model.Role;
import com.SimpleMVC.model.SecToken;
import com.SimpleMVC.model.User;
import com.SimpleMVC.orika.OrikaBeanMapper;
import org.slf4j.Logger;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.time.Clock;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static javax.ejb.TransactionAttributeType.REQUIRED;

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = REQUIRED)
public class UserServiceImpl implements UserService {

    @Inject
    private Logger logger;

    @PersistenceContext(unitName = "SimpleMVCJPA")
    private EntityManager em;

    @Inject
    private Clock clock;

    @Inject
    private OrikaBeanMapper mapper;

    @PermitAll
    @Override
    public UserDto findByLogin(String login) throws SimpleMVCException {
        try {
            return mapper
                    .map(em.createNamedQuery("User.findByLogin", User.class).setParameter("login", login).getSingleResult(), UserDto.class);
        } catch (NoResultException e) {
            String msg = String.format("Not found user: %s", login);
            logger.error(msg, e);
            throw new SimpleMVCException(msg);
        }
    }

    @RolesAllowed({Role.Name.SERVICE_ADMIN, Role.Name.MANAGER})
    @Override
    public UserDto save(UserDto userDto) throws SimpleMVCException {
        User user = mapper.map(userDto, User.class);
        if (user.getId() == null || em.find(User.class, user.getId()) == null) {
            em.persist(user);
        } else {
            em.merge(user);
        }
        em.flush();
        return mapper.map(user, UserDto.class);
    }

    @PermitAll
    @Override
    public Set<String> loadRoles(UserDto userDto) throws SimpleMVCException {
        User user = mapper.map(userDto, User.class);
        Set<String> roles = new HashSet<>();
        if (user.isAdmin()) {
            roles.add("ADMIN");
        }
        List<AgreementTypeCode> codes = em.createNamedQuery("Agreement.findAllCodes", AgreementTypeCode.class).setParameter("user", user)
                .getResultList();

        for (AgreementTypeCode code : codes) {
            if (code == AgreementTypeCode.AGENCY_AGREEMENT) {
                roles.add("OWNER");
            }
            if (code == AgreementTypeCode.RENT_AGREEMENT) {
                roles.add("RENTER");
            }
            if (code == AgreementTypeCode.SERVICE_AGREEMENT) {
                // ???
            }
        }
        return roles;
    }

    @PermitAll
    @Override
    public void saveToken(String token, UserDto userDto) throws SimpleMVCException {
        User user = mapper.map(userDto, User.class);

        SecToken secToken = new SecToken();
        secToken.setToken(token);
        secToken.setSalt(user.getSalt());
        secToken.setCreateDate(Date.from(clock.instant()));

        em.persist(secToken);
    }

    @PermitAll
    @Override
    public String getSalt(String token) throws SimpleMVCException {
        return em.find(SecToken.class, token).getSalt();
    }
}
