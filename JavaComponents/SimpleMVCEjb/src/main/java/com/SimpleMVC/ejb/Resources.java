package com.SimpleMVC.ejb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Clock;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

@ApplicationScoped
public class Resources {

    @Produces
    public Clock clock() {
        return Clock.systemDefaultZone();
    }

    @Produces
    public Logger logger(InjectionPoint injectionPoint) {
        return LoggerFactory.getLogger(injectionPoint.getMember().getDeclaringClass());
    }
}
