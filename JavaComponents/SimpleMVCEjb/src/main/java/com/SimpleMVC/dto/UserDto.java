package com.SimpleMVC.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
public class UserDto implements Serializable {

    private Integer id;

    private String name;

    private String login;

    private String password;

    private String groupId;

    private String salt;

    private String phone;

    private Integer codeTryCount;

    private Integer role;
}
