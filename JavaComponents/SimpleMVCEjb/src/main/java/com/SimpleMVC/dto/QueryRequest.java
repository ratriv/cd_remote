package com.SimpleMVC.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class QueryRequest {

    public static class Filter {

        public static final String ADDRESS = "address";

        public static final String SUBJECT_NAME = "subjectName";

        public static final String COMPANY_NAME = "companyName";

        public static final String BANK_NAME = "bankName";

        public static final String COUNTER_TYPE = "counterType";

        public static final String ACCOUNT = "account";

        public static final String SUBJECT_TYPE = "subjectType";

        public static final String CATEGORY = "category";

        public static final String NUMBER = "number";

        public static final String SIGNED = "signed";

        public static final String BUILT = "built";

        public static final String BIRTH = "birth";

        public static final String CREATED = "created";

        public static final String PAID = "paid";
    }

    public static QueryRequest create(String field, String name, Integer id, Date from, Date to) {
        QueryRequest request = new QueryRequest();
        request.setField(field);
        request.setName(name);
        request.setId(id);
        request.setFrom(from);
        request.setTo(to);
        return request;
    }

    private String field;

    private String name;

    private Integer id;

    private Date from;

    private Date to;
}
