package com.SimpleMVC.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the bank_details database table.
 */
@Getter
@Setter
@EqualsAndHashCode
public class BankDetailDto implements Serializable {

    private Integer id;

    private String address;

    private String banksName;

    private String bik;

    private String corrAccount;

    private String inn;

    private String kpp;

    private String note;

    private String payAccount;

    private String phone;

    private String transitAccount;

    private List<Integer> serviceCompanyBanks = new ArrayList<>();

    private List<Integer> subjectsBanks = new ArrayList<>();
}
