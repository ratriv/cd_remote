package com.SimpleMVC.exception;

import org.slf4j.Logger;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ExceptionHandler implements ExceptionMapper<Exception> {

    @Inject
    private Logger logger;

    @Override
    public Response toResponse(Exception exception) {
        logger.error("Exception", exception);
        ExceptionResponse response = new ExceptionResponse();
        response.setMessage(exception.getMessage());
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(response).build();
    }
}
