package com.SimpleMVC.rest;

import org.slf4j.Logger;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("/message")
public class MessageRestController {

    @Inject
    private Logger logger;

    @GET
    @Path("reflect/{param}")
    public Response printMessage(@PathParam("param") String msg) {
        String result = "Restful parameter reflection test : " + msg;
        logger.debug(result);

        logger.info("INFO from MessageRestController");
        logger.info("Exception", new Exception("FATAL from MessageRestController"));

        return Response.ok().entity(result).build();
    }
}
