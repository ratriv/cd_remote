package com.SimpleMVC.rest;

import com.SimpleMVC.commons.SimpleMVCException;
import com.SimpleMVC.local.UserService;
import com.SimpleMVC.dto.UserDto;
import com.SimpleMVC.dto.legacy.Login;
import com.SimpleMVC.dto.legacy.LoginResponse;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;

import java.time.Clock;
import java.util.Base64;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/login")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LoginController implements BaseSimpleMVCController {

    @Inject
    private UserService manager;

    @Inject
    private Clock clock;

    @Inject
    private Logger logger;

    @PermitAll
    @POST
    @Path("")
    public Response doLogin(Login login) {
        UserDto user = null;

        try {
            logger.info("Searching for the user :" + login.getLogin());
            user = manager.findByLogin(login.getLogin());

            logger.info("Existing user found :" + user.toString());
        } catch (SimpleMVCException e) {
            logger.error(e.getMessage(), e);
        }

        if (user == null || user.getId() == 0) { // || !BCrypt.checkpw(login.getPassword(), user.getPassword())) {
            return Response.status(Status.UNAUTHORIZED).entity(user).build();
        }
        final String token = buildToken(user, clock.millis());
        String message = null;
        try {
            manager.saveToken(token, user);
            logger.info("Got token :" + token);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = e.getMessage();
        }
        LoginResponse response = new LoginResponse(token);
        response.setMessage(message);
        return Response.status(Status.OK).entity(response).build();
    }

    private String buildToken(UserDto user, long now) {
        return Jwts.builder()
            .claim("user", user.getId())
            .claim("created", clock.millis())
            .setSubject(user.getLogin())
            .setIssuedAt(new Date(now))
            .setExpiration(new Date(now + TimeUnit.HOURS.toMillis(1)))
            .signWith(SignatureAlgorithm.HS512, Base64.getEncoder().encodeToString(user.getSalt().getBytes()))
            .compact();
    }
}
