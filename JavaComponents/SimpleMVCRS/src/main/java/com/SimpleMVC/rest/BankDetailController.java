package com.SimpleMVC.rest;

import com.SimpleMVC.dto.BankDetailDto;
import com.SimpleMVC.local.BankDetailService;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/bankDetails")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BankDetailController extends AbstractController<BankDetailDto, Integer, BankDetailService> {

    @Inject
    private BankDetailService bankDetailService;

    @Override
    protected BankDetailService getService() {
        return bankDetailService;
    }
}
