package com.SimpleMVC.rest;

import com.SimpleMVC.commons.SimpleMVCException;
import com.SimpleMVC.dto.QueryRequest;
import com.SimpleMVC.local.CrudService;

import java.util.Date;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

public abstract class AbstractController<E, ID, S extends CrudService<E, ID>> {

    protected abstract S getService();

    @RolesAllowed("ADMIN")
    @GET
    @Path("/")
    public List<E> getAll(@QueryParam("field") String field, @QueryParam("name") String name,
                          @QueryParam("id") Integer id, @QueryParam("from") Date from, @QueryParam("to") Date to) throws
        SimpleMVCException {
        return getService().list(QueryRequest.create(field, name, id, from, to));
    }

    @RolesAllowed("ADMIN")
    @GET
    @Path("/{id}")
    public E getById(@PathParam("id") ID id) throws SimpleMVCException {
        return getService().getById(id);
    }

    @RolesAllowed("ADMIN")
    @GET
    @Path("/last")
    public E getLast() throws SimpleMVCException {
        return getService().last();
    }

    @RolesAllowed("ADMIN")
    @POST
    @Path("/")
    public E create(E dto) throws SimpleMVCException {
        return getService().create(dto);
    }

    @RolesAllowed("ADMIN")
    @DELETE
    @Path("/{id}")
    public boolean delete(@PathParam("id") ID id) throws SimpleMVCException {
        return getService().delete(id);
    }
}
