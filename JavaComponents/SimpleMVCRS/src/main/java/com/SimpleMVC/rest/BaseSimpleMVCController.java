package com.SimpleMVC.rest;

public interface BaseSimpleMVCController {

    public static final String OK = "{\"status\":\"ok\"}";
    public static final String FAIL = "{\"status\":\"fail\"}";

}
