package com.SimpleMVC.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest")
public class SimpleMVCApplication extends Application {
}
