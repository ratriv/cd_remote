package com.SimpleMVC.security;

import com.SimpleMVC.local.UserService;
import com.SimpleMVC.dto.UserDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 * This interceptor verify the access permissions for a user
 * based on username and passowrd provided in request
 */
@Provider
@Priority(Priorities.AUTHORIZATION)
public class SecurityInterceptor implements ContainerRequestFilter {

    @Inject
    private UserService manager;

    @Inject
    private Logger logger;

    private String getToken(ContainerRequestContext context) {
        String headerValue = context.getHeaderString(HttpHeaders.AUTHORIZATION);
        logger.debug("headerValue " + headerValue);
        if (headerValue == null || headerValue.trim().isEmpty()) {
            return null;
        }

        final MultivaluedMap<String, String> headers = context.getHeaders();
        final List<String> authorization = headers.get(HttpHeaders.AUTHORIZATION);
        if (authorization == null || authorization.isEmpty()) {
            return null;
        }
        final String bearer = authorization.get(0);

        if (bearer.startsWith("Bearer ")) {
            return bearer.substring(7);
        }
        return null;
    }

    @Override
    public void filter(ContainerRequestContext context) throws IOException {
        UserDto currentUser = null;
        Set<String> roles = new HashSet<>();
        String login = null;
        String token = getToken(context);

        if (token != null) {
            String salt = "";
            try {
                salt = manager.getSalt(token);
                String secretBase64 = Base64.getEncoder().encodeToString(salt.getBytes());

                Claims claims = Jwts.parser().setSigningKey(secretBase64).parseClaimsJws(token).getBody();
                login = claims.getSubject();

                currentUser = manager.findByLogin(login);
                roles.addAll(manager.loadRoles(currentUser));
                roles.add("ADMIN");
            } catch (Exception e) {
                throw new NotAuthorizedException("Unauthorized: Token is corrupted " + token + ", " + salt,
                    Response.status(Response.Status.UNAUTHORIZED));
            }
        }

        if (currentUser != null) {
            context.setSecurityContext(
                new Authorizer(roles, new SimpleMVCUser(currentUser.getId(), login), context.getSecurityContext().isSecure()));
        }
    }
}
