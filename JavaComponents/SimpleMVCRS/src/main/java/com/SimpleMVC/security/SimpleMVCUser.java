package com.SimpleMVC.security;

import javax.security.auth.Subject;
import java.security.Principal;

public class SimpleMVCUser implements Principal {

    private int id;
    private String name;

    public SimpleMVCUser(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean implies(Subject subject) {
        return false;
    }
}
