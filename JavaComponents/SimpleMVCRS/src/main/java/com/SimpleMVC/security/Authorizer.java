package com.SimpleMVC.security;

import java.security.Principal;
import java.util.Set;
import javax.ws.rs.core.SecurityContext;

public class Authorizer implements SecurityContext {

    private Set<String> roles;
    private SimpleMVCUser user;
    private boolean isSecure;

    public Authorizer(Set<String> roles, final SimpleMVCUser user,
                      boolean isSecure) {
        this.roles = roles;
        this.user = user;
        this.isSecure = isSecure;
    }

    @Override
    public Principal getUserPrincipal() {
        return user;
    }

    @Override
    public boolean isUserInRole(String role) {
        return roles.contains(role);
    }

    @Override
    public boolean isSecure() {
        return isSecure;
    }

    @Override
    public String getAuthenticationScheme() {
        return "Your Scheme";
    }
}
