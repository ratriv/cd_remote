package com.SimpleMVC.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tokens")
@Getter
@Setter
@EqualsAndHashCode
public class SecToken implements Serializable {

    @Id
    private String token;

    @Column(name = "cdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    private String salt;
    
}
