package com.SimpleMVC.model;

public enum AgreementTypeCode {

    AGENCY_AGREEMENT("Agency Agreement"),
    RENT_AGREEMENT("Rent Agreement"),
    SERVICE_AGREEMENT("Service Agreement");

    AgreementTypeCode(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }
}
