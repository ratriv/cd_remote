package com.SimpleMVC.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity(name = "User")
@Table(name = "SimpleMVC_user")
@NamedQueries({
        @NamedQuery(name = "User.findByLogin", query = "SELECT u FROM User u WHERE u.login = :login")
})
@Getter
@Setter
@EqualsAndHashCode
public class User implements Serializable, ObjectWithId {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "group_id", columnDefinition = "char(1)")
    private String groupId;

    @Column(name = "salt")
    private String salt;

    @Column(name = "phone")
    private String phone;

    @Column(name = "code_try_count")
    private Integer codeTryCount;

    @Column(name = "role_code")
    @Enumerated(EnumType.ORDINAL)
    private Role role;

    public boolean isAdmin() {
        return "1".equals(groupId);
    }   

    public Role getRole() {
        if (role == null)
            return Role.UNDEF;
        return role;
    }
}
