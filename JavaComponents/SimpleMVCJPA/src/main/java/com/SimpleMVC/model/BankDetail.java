package com.SimpleMVC.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * The persistent class for the bank_details database table.
 */
@Entity(name = "BankDetail")
@Table(name = "bank_details")
@NamedQueries( {
    @NamedQuery(name = "BankDetail.findAll", query = "SELECT b FROM BankDetail b"),
    @NamedQuery(name = "BankDetail.findByAddress", query = "SELECT b FROM BankDetail b WHERE b.address LIKE :address"),
    @NamedQuery(name = "BankDetail.getLast", query = "SELECT s FROM BankDetail s WHERE s.id = ( SELECT MAX(ss.id) FROM BankDetail ss)"),
    //@NamedQuery(name = "BankDetail.findBySubjectName", query = "SELECT b FROM BankDetail b LEFT JOIN b.subjectsBanks sb"
    //    + " LEFT JOIN sb.msubject m WHERE m.name LIKE :name"),
    @NamedQuery(name = "BankDetail.findByBankName", query = "SELECT b FROM BankDetail b WHERE b.banksName LIKE :name"),
    @NamedQuery(name = "BankDetail.findByAccount", query = "SELECT b FROM BankDetail b WHERE b.payAccount LIKE :name"),
    //@NamedQuery(name = "BankDetail.findBySigned", query = "SELECT b FROM BankDetail b LEFT JOIN b.subjectsBanks sb"
    //    + " LEFT JOIN sb.msubject s LEFT JOIN s.objectAgreements oa LEFT JOIN oa.magreement m WHERE m.signed BETWEEN :dateFrom AND :dateTo")
})
@Getter
@Setter
@EqualsAndHashCode
public class BankDetail implements Serializable, ObjectWithId {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "address")
    private String address;

    @Column(name = "banks_name")
    private String banksName;

    @Column(name = "bik")
    private String bik;

    @Column(name = "corr_account")
    private String corrAccount;

    @Column(name = "inn")
    private String inn;

    @Column(name = "kpp")
    private String kpp;

    @Column(name = "note")
    private String note;

    @Column(name = "pay_account")
    private String payAccount;

    @Column(name = "phone")
    private String phone;

    @Column(name = "transit_account")
    private String transitAccount;

    /*
    @OneToMany(mappedBy = "bankDetail")
    private List<ServiceCompanyBank> serviceCompanyBanks;

    @OneToMany(mappedBy = "bankDetail")
    private List<SubjectsBank> subjectsBanks;


    public ServiceCompanyBank addServicecompanyBank(ServiceCompanyBank serviceCompanyBank) {
        getServiceCompanyBanks().add(serviceCompanyBank);
        serviceCompanyBank.setBankDetail(this);
        return serviceCompanyBank;
    }

    public ServiceCompanyBank removeServicecompanyBank(ServiceCompanyBank serviceCompanyBank) {
        getServiceCompanyBanks().remove(serviceCompanyBank);
        serviceCompanyBank.setBankDetail(null);
        return serviceCompanyBank;
    }

    public SubjectsBank addSubjectsBank(SubjectsBank subjectsBank) {
        getSubjectsBanks().add(subjectsBank);
        subjectsBank.setBankDetail(this);
        return subjectsBank;
    }

    public SubjectsBank removeSubjectsBank(SubjectsBank subjectsBank) {
        getSubjectsBanks().remove(subjectsBank);
        subjectsBank.setBankDetail(null);
        return subjectsBank;
    }

    */
}
