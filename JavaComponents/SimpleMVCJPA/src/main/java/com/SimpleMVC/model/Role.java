package com.SimpleMVC.model;

import java.util.EnumSet;

public enum Role {

    /**
     * System administrator.
     */
    SYSTEM_ADMIN,

    /**
     * Service administrator.
     */
    SERVICE_ADMIN,

    /**
     * Manager.
     */
    MANAGER,

    /**
     * Operator.
     */
    OPERATOR,

    /**
     * Owner.
     */
    OWNER,

    /**
     * Renter.
     */
    RENTER,

    /**
     * Undefined.
     */
    UNDEF;

    public static final EnumSet<Role> SAD_MNG = EnumSet.of(Role.SERVICE_ADMIN, Role.MANAGER);

    public boolean isServiceAdminOrManager() {
        return this == SERVICE_ADMIN || this == MANAGER;
    }

    public static class Name {
        public static final String SYSTEM_ADMIN = "SYSTEM_ADMIN";
        public static final String SERVICE_ADMIN = "SERVICE_ADMIN";
        public static final String MANAGER = "MANAGER";
        public static final String OPERATOR = "OPERATOR";
        public static final String OWNER = "OWNER";
        public static final String RENTER = "RENTER";
    }
}
